;***********************************************************************************************************************************
; Escuela de Ingenieria Electrica
; Curso Microprocesadores
; IE 0623
; Tarea 6
; Natalia Bolanos Murillo B61127
; Ultima vez modificado 14/07/20
;***********************************************************************************************************************************
;
;***********************************************************************************************************************************
#include registers.inc
;***********************************************************************************************************************************
EOM          	EQU $FF
CR              EQU $0A
LF              EQU $0D
NP          	EQU $1A

;
;***********************************************************************************************************************************
;                                       DECLARACION DE ESTRCUTURAS DE DATOS
;***********************************************************************************************************************************
                org $1010

Nivel_Prom:     ds 2          ;0
Nivel:          ds 1          ;2
Volumen:        ds 1          ;3
Cont_RTI:       ds 1          ;4
BCD_L:          ds 1          ;5
LOW:            ds 1          ;6
Puntero1:       ds 2          ;7
Puntero2:       ds 2          ;9
Banderas:       ds 1          ;11
                org $1070


MSG_op:         db NP
                FCC "            UNIVERSIDAD DE COSTA RICA"
                db CR
                db LF
                FCC "          Escuela de Ingenieria Electrica"
                db CR
                db LF
                FCC "                 Microprocesadores"
                db CR
                db LF
                FCC "                      IE0623"
                db CR
                db LF
                FCC "                     Volumen: "
Vol_c:      	ds 1
Vol_d:      	ds 1
Vol_u:      	ds 1
                db EOM
                
MSG_alerta:     db CR
                db LF
		FCC "              Alarma: El nivel esta bajo"
                db EOM
MSG_vaciar:     db CR
                db LF
		FCC "              Tanque vaciando, bomba apagada"
                db EOM

;***********************************************************************************************************************************
;                                          RELOCALIZACION DE VECTORES DE INTERRUPCION
;***********************************************************************************************************************************
                org $FFF0
                dw RTI_ISR
                org $FFD2
                dw ATD_ISR
                org $FFD4
                dw SC1_ISR
;***********************************************************************************************************************************
;***********************************************************************************************************************************
;                  `                           PROGRAMA PRINCIPAL
;***********************************************************************************************************************************
;***********************************************************************************************************************************

                org $2000
                ; LED
                MOVB #$01,DDRB
                BSET DDRJ,$02
                BCLR PTJ,$02
                ; SCI
                MOVW #39,SC1BDH
                MOVB #$00,SC1CR1
                MOVB #$88,SC1CR2

                ; RTI
                MOVB #$54,RTICTL             ; Se carga base de tiempo para RTI

                ; ATD
                MOVB #$C2,ATD0CTL2
                LDAA #160
loop_ATD:
                DBNE A,loop_ATD         ; Retardo para encender el convertidor

                MOVB #$30,ATD0CTL3
                MOVB #$10,ATD0CTL4
                MOVB #$80,ATD0CTL5

                LDS #$4000
                CLI
                
                LDX #MSG_op
                STX Puntero1
                LDAA SC1SR1              ; Se inicia transmision SCI
                MOVB #$0C,SC1DRL
                BSET CRGINT,$80
                CLR Banderas
loop_pp:
                JSR Calcular            ; Se obtiene el valor del volumen
                LDAA Volumen
                CMPA #17                ; Se revisa su se debe desplegar mensaje de vaciado o alerta
                BMI Alerta
                CMPA #32
                BPL No_alerta
cont_Comp:
                CMPA #95
                BPL Vaciar
                BRA loop_pp
Alerta:
                BSET Banderas,$01
                BCLR Banderas,$02
                MOVB #$01,PORTB
                BRA loop_pp
No_alerta:
                BCLR Banderas,$01
                CLR PORTB
                BRA cont_comp
Vaciar:
                BSET Banderas,$02
                BRA loop_pp
                
                

;***********************************************************************************************************************************
;                                          SUBRUTINA CALCULAR
;***********************************************************************************************************************************
CALCULAR:
                LDD Nivel_Prom
                LDY #20
                EMUL
                LDX #1023
                EDIV
                TFR Y,D
                STAB Nivel      ; Nivel en metros
                LDAA #7         ; Area de la base
                MUL             ; Volumen = area base * nivel
                STAB Volumen
                CMPB #105
                BMI Vol_correcto    ; Si el volumen es mayor al maximo, lo que indicaria que se supera la altura del tanque
                LDAB #105	   ; Se guarda en volumen el valor maximo posible
Vol_correcto:
		LDX #100       	; Separar centenas decenas y unidades para imprimirlos en el mensaje
                IDIV          	; En X centenas, en D residuo
                PSHD
                TFR X,D
                ADDB #$30
                STAB VOL_C
                PULD
                LDX #10
                IDIV
                ADDB #$30
                STAB VOL_U
                TFR X,D
                ADDB #$30
                STAB VOL_D
                RTS

;***********************************************************************************************************************************
;                                          SUBRUTINA SC1_ISR
;***********************************************************************************************************************************
SC1_ISR:
                LDAA SC1SR1
                LDX Puntero1
                LDAA 0,X
                INX
                CMPA #EOM
                BNE cont_t
                BRSET Banderas,$10,int_off      ; Hace branch si ya se desplego el mensaje de alerta o vaciado
                BRSET Banderas,$01,m_alerta     ; Hace brnach si se debe desplegar mensaje de alerta
                BRSET Banderas,$02,m_vaciar     ; Hace brnach si se debe desplegar mensaje de vaciado
                BRA int_off
m_alerta:
                MOVW #MSG_alerta,Puntero1
                BSET Banderas,$10
                BRA end_SCI
m_vaciar:
                MOVW #MSG_vaciar,Puntero1
                BSET Banderas,$10
                BRA end_SCI
int_off:                                        ; Se apaga la interrupcion
                BCLR SC1CR2,$40
                BRA end_SCI
cont_t:
                STAA SC1DRL                     ; Se inicia transmision del mensaje
                STX Puntero1
end_SCI:
                Rti

;***********************************************************************************************************************************
;                                          SUBRUTINA RTI_ISR
;***********************************************************************************************************************************

RTI_ISR:
                DEC CONT_RTI
                BNE SALIR_RTI
                MOVB #100,CONT_RTI
                LDAA SC1SR1
                MOVB #$1A,SC1DRL
                LDX #MSG_OP            ; Se carga mensaje inicial en el puntero
                STX Puntero1
                BSET SC1CR2,$40        ; Se habilitan interrupciones SCI
                BCLR Banderas,$10
SALIR_RTI:
                BSET CRGFLG,$80         ; Borrar bandera
		RTI

;***********************************************************************************************************************************
;                                          SUBRUTINA ATD_ISR
;***********************************************************************************************************************************
ATD_ISR:
                LDX #6                  ; Se calcula el promedio de las mediciones
                LDD ADR00H              ; Se suman las 6 lecturas
                ADDD ADR01H
                ADDD ADR02H
                ADDD ADR03H
                ADDD ADR04H
                ADDD ADR05H
                IDIV                    ; Se divide la suma entre 6
                STX Nivel_Prom
                MOVB #$80,ATD0CTL5
                RTI
                
                
