;***************************************************************************
;                       ESCUELA DE INGENIERIA ELECTRICA
;                              CURSO MICROPROCESADORES
;                                    IE 0423
;                               TAREA 2 PROBLEMA 3
;                            NATALIA BOLANOS B61127
;****************************************************************************
;                       NUMEROS DIVISIBLES ENTRE 4
; En este problema se leen datos de la tabla DATOS que se encuentra a partir de
; la direccion $1100, en el caso de ser divisibles entre 4, se copian al
; arreglo DIV4 que esta partir de la direccion $1200.
; Estos datos son numeros con signo por lo que se calcula primero su magnitud
; en caso de ser negativos.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
                org $1000
L:               db $4 ;  Constante con la cantidad de datos a leer
CANT4:          ds 1 ; Variable para guardar el total de numeros divisibles entre 4
                org $1100
DATOS:          db $84,$85,$44,$59 ; Tabla de datos
                org $1200 
DIV4:           ds #255 ;  Arreglo para los numeros divisibles entre 4
;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************
                ORG $2000
INICIO:
        LDX #DATOS
        LDY #DIV4
        CLRA
        CLR CANT4
LOOP:
        LDAB L
        CBA ; Si ya se recorrieron todos los datos se va a FIN
        BEQ FIN
        LDAB A,X ; Se lee el dato
        BPL POSITIVO ; Si el bit mas significativo es 0, se va a POSITIVO
NEGATIVO:
        NEGB ; Se hace el complemento a la base 2 pasa obtener la magnitud
POSITIVO:
        RORB ;Rotacion a la derecha para revisar ultimo bit
        BCC DIV2 ; Si el bit es 0, es divisible entre 2
        INCA
        BRA LOOP
DIV2:
	RORB ;Rotacion a la derecha para revisar ultimo bit
        BCC DIV ; Si el bit es 0, es divisible entre 4
        INCA
        BRA LOOP
DIV:
        LDAB CANT4
        MOVB A,X,B,Y  ; Se mueve el dato al DIV4
        INCB
        INCA
        STAB CANT4 ; Se actualiza la cantidad de numeros divisibles entre 4
        BRA LOOP
FIN:
        BRA FIN


        



















