;***************************************************************************
;                       ESCUELA DE INGENIERIA ELECTRICA
;                              CURSO MICROPROCESADORES
;                                    IE 0423
;                                     TAREA 2 PROBLEMA 2
;                            NATALIA BOLANOS B61127
;****************************************************************************
;                       DATOS Y MASCARAS
; El programa se encarga de leer los datos de la tabla DATOS, hacer una XOR entre
; el dato y la mascara leida de la tabla MASCARAS y si el resultado es negativo
; se debe guardar en el arreglo a partir de NEGAT.
; Las mascaras se deben aplicar de la siguiente forma:
; ultimo dato con la primer mascara, penultimo datos con la segunda mascara y
; asi sucesivamente.
; Se termina el programa cuando se llega a la ultima posicion de las mascaras
; o cuando se llegue a la primera de los datos.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
                org $1000
RESPALDO:       ds $2 ; Variable para respaldar el valor del indice Y
TEMP:           ds $4 ; Variable para guardar la direccion donde se debe guardar el siguiente resultado
                org $1050
DATOS:          db $10,$F4,$e3,$45,$b8,$80 ; Tabla de datos
                org $1150
MASCARAS:       db $10,$21,$9f,$22,$F1,$FE ; Tabla de mascaras
                org $1300
NEGAT:          dw 1000 ; Arreglo de resultados negativos
;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************
                org $2000
INICIO:
        LDY #NEGAT
        STY TEMP
        LDX #DATOS-1
        LDY #MASCARAS-1
        LDAA #$80
ULT_DATO:
        CMPA 1,+X ; Se revisa si se llego al final de la tabla datos
        BNE ULT_DATO
LOOP:
        INY
        CPX #$104f ; Se revisa si ya se recorrieron todos los datos
        BEQ FIN
        LDAA 0,Y ; Se carga el valor guardado en Y en A
        EORA 1,X- ; Se realiza XOR entre el dato y la mascara
        BPL LOOP ; En el caso de no ser negativo se vuelve al LOOP
ESNEG:
        STY RESPALDO ; Se respalda la posicion por la que va en la tabla de mascaras
        LDY TEMP
        STAA 1,Y+ ; Guarda el resultado en el arreglo
        STY TEMP
        LDY RESPALDO ; Se devuleve a Y en valor respaldado (posicion en la tabla de mascaras)
        LDAA 0,Y 
          CMPA #$FE ; Se revisa si se lego a la ultima posicion de las mascaras
          BNE LOOP
FIN:
        BRA FIN