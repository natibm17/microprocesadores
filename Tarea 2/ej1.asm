;***************************************************************************
;                       ESCUELA DE INGENIERIA ELECTRICA
;                            CURSO MICROPROCESADORES
;                                    IE 0423
;                              TAREA 2 PROBLEMA 1
;                            NATALIA BOLANOS B61127
;****************************************************************************
;                       	CONVERSIONES
; En este programa se realiza la conversion de un numero binario de 12 bits a BCD
; y viceversa un numero en BCD de 16 bits a binario.
; El numero binario a convertir se encuentra en las posiciones de memoria $1000-$1001
; y el numero en BCD se encuentra en las posiciones $1002-$1003
; El resultado en BCD se guarda en la variable NUM_BCD a partir de la posicion
; memoria $1010 y el resultado en binario se guarda en NUM_BIN a partir de la posicion
; de memoria $1020.
; El prgrama primero realiza la conversion BIN-BCD y luego BCD-BIN.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
                org $1000
BIN:            dw 4093 ; Constante con el numero en binario que se debe convertir a BCD
BCD             dw #$9597 ; Constante con el numero en BCD que se debe convertir a binario
TEMP            ds 1    ; Variable usada en BIN-BCD para guaradar temporalmente la parte baja del
			; numero a analizar y en BCD-BIN es utilizada para guardar el numero de potencia de 10 correspondiente al nibble analizado
                org $1010
NUM_BCD:        ds 2 ; Variable donde se guarda el resultado de la conversion BIN-BCD
                org $1020
NUM_BIN:        ds 2 ; Variable donde se guarda el resultado de la conversion BCD-BIN

;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************
                ORG $2000
CONVERSIONES:
        CLR NUM_BCD
        CLR NUM_BCD+1
        CLR NUM_BIN
        CLR NUM_BIN+1
        LDX #$4
        LDY #$B
        LDD BIN
BIN_BCD:
        LSLD ; Se desplaza el numero binario a la izquierda hasta que los 12 bits queden en las posiciones mas significativas
        DBNE X,BIN_BCD ; Se pasa a LOOP cuando ya se desplazaron todos los bits a la posicion deseada
LOOP:
        LSLD ; Se hace un desplazamiento en el numero binario
        ROL NUM_BCD+1 ; Se desplaza la parte menos significativa
        ROL NUM_BCD ; Se desplaza la parte mas significativa
        TFR D,X ; Se respalda el numero en binario desplazado en X
        LDD NUM_BCD
        ANDB #$0F ; Se carga en B la parte baja del byte menos significativo del numero en BCD
        CMPB #$5 ; Si es menor a 5 el valor se mantiene
        BMI MENOR
        ADDB #$3 ; Se le suma 3 en caso de ser mayor o igual a 5
MENOR:
        STAB TEMP ; Se respalda la parte mas baja del numero en BCD
        LDD NUM_BCD
        ANDB #$F0 ; Se carga en B la parte alta del byte menos significativo del numero en BCD
        CMPB #$50 ; Si esa parte es menor que 5 el valor se mantiene
        BMI MENOR2
        ADDB #$30 ; Si es mayor o igual, se le suma 3
MENOR2:
        ADDB TEMP ; Se suman las dos partes menos significativas del numero
        STAB NUM_BCD+1 ; Se guarda el resultado de la suma en la parte menos significatica del numero en BCD
        ANDA #$0F ; Se carga en A la parte baja del byte mas significativo
        CMPA #$5 ; Si esa parte es menor a 5 el valor se mantiene
        BMI MENOR3
        ADDA #$3 ; Si esa parte es mayor o igual a 5 se le suma 3
MENOR3:
        LDAB NUM_BCD
        ANDB #$F0 ; Se carga en B la parte alta del byte mas significativo del numero en BCD
        ABA ; Se suman la parte mas significativa y la menos significativa
        STAA NUM_BCD ; Se guarda el resultado
        TFR X,D ; Se devuelve el valor del numero binario desplazado a D
        CPY #$0
        DBNE Y,LOOP ; Si solo falta el ultimo bit del numero binario por desplazar se deja de iterar en LOOP
        LSLD ; Se realiza el ultimo desplzamiento
        ROL NUM_BCD+1 ; Se guarda la parte baja del resultado final
        ROL NUM_BCD ; Se guarda la parte alta del resultado final

BCD_BIN:
        LDD BCD ; Se carga en D el numero en BCD
        TFR D,X ; Se respalda el numero en BCD en X
        LDY #$1
        STY TEMP
        ANDB #$0F ; Se carga en B la parte baja del byte menos significativo
        STAB NUM_BIN+1 ; Se guarda en NUM_BIN el resultado de procesar los primeros 4 bits
        TFR X,D ; Se devuelve a D el numero en BCD
        LDY #$4
LOOP2:
        LSRD ; Se desplaza el numero en BCD hacia la derecha
        DBNE Y,LOOP2 ; Cuando itera en LOOP2 hasta que se hayan desplazado 4 bits
        TFR D,X ; Se respalda en X el numero en BCD desplazado
        ANDB #$0F ; Se carga en B la parte baja del byte menos significativo
        TFR B,Y
        LDAA #10
        LDAB TEMP+1
        MUL ; Se obtiene la potencia de 10 correspondiente a la decada
        STD TEMP ; Se guarda la potencia obtenida en la variable temporal
        EMUL ; Se multiplican los bits seleccionados por la potencia de 10 correspondiente
        ADDD NUM_BIN ; Se suma el resultado de la multiplicacion con la suma de las anteriores
        STD NUM_BIN ; Se guarda el resultado final en NUM_BIN
        LDY #$4
        TFR X,D
        LDX TEMP
        CPX #1000 ; Cuando la variable TEMP llega a 1000 significa que ya se ha procesado la ultima decada del numero en BCD
        BNE LOOP2
FIN:
        BRA *
        

