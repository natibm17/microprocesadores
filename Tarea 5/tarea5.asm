;***************************************************************************
;                       Escuela de Ingenieria Electrica
;                            Curso Microprocesadores
;                                    IE 0423
;                                    Tarea 5
;                            Natalia Bolanos B61127
;                        Ultima vez modificado 28-06-20
;****************************************************************************
; El programa simula una linea de produccion de tornillos, para esto se ingresa
; al modo config con el dip switch 1 en on y por medio del teclado se ingresa un
; valor entre 20 y 90. Se puede pasar al modo run poniendo el dip switch 1 en off.
; En los displays 3 y 4 se muestra la cantidad de tornillos dispensados y en los
; displays 1 y 2 se muestra la cantidad de paquetes acumulados.
; Con el boton ph0 se puede borrar la catidad de tornillos y con el ph1 la de paquetes
; Con los botones ph2 y ph3 se puede bajar y subir el brillo respectivamente.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
#include registers.inc

EOM:            equ $0
VMAX:           equ $14d        ; 333


                org $1000
BANDERAS:       ds 1 ; Variable de 1 byte utilizada para guardar las banderas Camb_Mode, Mod_Actual, Array_OK, TCL_Leida, TCL_Lista
MAX_TCL         db 2 ; Constante de 1 byte que indica el tamano maximo del arreglo Num_Array
Tecla:          ds 1 ; Variable de 1 byte utilizada para almacenar el valor a guardar en el arreglo
Tecla_IN        ds 1 ; Variable de 1 byte donde la subrutina FORMAR_ARRAY lee el valor de la tecla presionada
Cont_Reb:       ds 1 ; Variable de 1 byte utilizada como contador de rebotes
Cont_TCL:       ds 1 ; Variable de 1 byte utilizada para almacenar el numero de tecla en Num_Array
Patron:         ds 1 ; Variable de 1 byte utilizada para cargar distintos patrones en el puerto A
Num_Array:      ds 2 ; Arreglo de 6 bytes donde se guardan los valores ingresados
Cuenta:         ds 1 ; Variable de 1 byte que lleva la cuenta de tornillos dispensados
AcmPQ:          ds 1 ; Variable de 1 byte que lleva la cuenta de paquetes acumulados
CantPQ:         ds 1 ; Variable de 1 byte con la cantidad de tornillos por paquete
TIMER_CUENTA:   ds 2 ; Variable de 1 byte utilizada para la temporizacion del aumento de Cuenta
LEDS:           ds 1 ; Variable de 1 byte que guarda el estado de los LEDs a ser desplegado
BRILLO:         ds 1 ; Variable de 1 byte que controla el brillo de la pantalla de 7 segmentos
CONT_DIG:       ds 1 ; Variable de 1 byte utilizado para la multiplexacion de la pantalla de 7 segmentos
CONT_TICKS:     ds 1 ; Variable de 1 byte utilizada para la multiplexacion de la pantalla de 7 segmentos
DT:             ds 1 ; Variable de 1 byte que maneja el ciclo de trabajo de encendido para los d\'igitos y los LEDs
BIN1:           ds 1 ; Variable de 1 byte que guarda el numero en bianrio que se debe desplegar en DISP3 y DISP4 de la pantalla de 7 segmentos
BIN2:           ds 1 ; Variable de 1 byte que guarda el numero en bianrio que se debe desplegar en DISP1 y DISP2 de la pantalla de 7 segmentos
BCD_L:          ds 1 ; Variable de 1 byte donde la subrutina BIN_BCD guarda el resultado en BCD
                org $1017
BCD1:           ds 1 ; Variable de 1 byte donde la subrutina Conv_BIN_BCD guarda el resultado de la conversion de BIN1 a BCD
BCD2:           ds 1 ; Variable de 1 byte donde la subrutina Conv_BIN_BCD guarda el resultado de la conversion de BIN2 a BCD
DISP1:          ds 1 ;Variable de 1 byte que guarda el valor a desplegar en el display 1 de la pantalla de 7 segmentos
DISP2:          ds 1 ; Variable de 1 byte que guarda el valor a desplegar en el display 2 de la pantalla de 7 segmentos.
DISP3:          ds 1 ; Variable de 1 byte que guarda el valor a desplegar en el display 3 de la pantalla de 7 segmentos
DISP4:          ds 1 ; Variable de 1 byte que guarda el valor a desplegar en el display 4 de la pantalla de 7 segmentos
CONT_7SEG:      ds 2 ; Varibale de 1 byte utilizada para el refrescamiento de los valores a desplegar en la pantalla de 7 segmentos
Cont_Delay:     ds 1 ; Variable  de 1 byte utilizada para el funcionamiento de la subrutina Delay
D2mS:           db 100 ; Constante de 1 byte utilizada para el retardo de 2 ms
D260uS:         db 13  ; Constante de 1 byte utilizada para el retardo de 260 us.
D40uS:          db 2   ; Constante de 1 byte utilizada para el retardo de 40 us

Clear_LCD:      db $01 ; Constante de 1 byte con comando para borrar la LCD.
ADD_L1:         db $80 ; constante igual a Adress linea 1 lcd
ADD_L2:         db $C0 ; constante igual a Adress linea 2 lcd
                org $1030
Teclas:         db $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E ; Tabla con los valores de las teclas
                org $1040
Segment:        db $3F,$06,$5B,$4F,$66,$6D,$7D,$07,$7F,$6F ; Tabla para conversion BCD a 7 segmentos
                org $1050
iniDsp:         db $04,$28,$28,$06,$0C ; Tabla de comandos para iniciar la comunicacion con la pantalla LCD
                org $1060
Msg_config_L1:          fcc "MODO CONFIG"
                db EOM
Msg_config_L2:          fcc "Ingrese CantPQ"
                db EOM
Msg_run_L1:          fcc "MODO RUN"
                db EOM
Msg_run_L2:          fcc "AcmPQ CUENTA"
                db EOM

                org $3E70
                dw RTI_ISR
                org $3E4C
                dw PTH_ISR
                org $3E66
                dw OC4_ISR

;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************
                org $2000
;****************************************************************************
;              CONFIGURACION DE HARDWARE
;****************************************************************************
        ; Puerto E
        BSET DDRE,$04               ; Rele

        ; Puerto H
        MOVB #$0C,PIEH       	; Se habilita interrupciones al puerto H
        MOVB #$0,PPSH           ; Flanco negativo

        ; RTI
        MOVB #$23,RTICTL        ; Se carga base de tiempo
        BSET CRGINT,$80         ; Se habilitan interrupciones rti

        ; Teclado
        MOVB #$F0,DDRA      	; Parte alta es salida, parte baja es entrada
        BSET PUCR,$01         	; pullup

        ; LEDS
        MOVB #$FF,DDRB
        BSET DDRJ,$02
        BSET PTJ, $02

        ; 7 SEG
        MOVB #$0F,DDRP
        MOVB #$0F,PTP

        ; OC4
        MOVB #$90,TSCR1
        MOVB #$03,TSCR2
        MOVB #$10,TIOS
        MOVB #$10,TIE
        MOVB #$01, TCTL1
        MOVB #$00, TCTL2


        LDD TCNT
        ADDD #60
        STD TC4

        MOVB #$FF,DDRK
        
        ; Definir puntero de pila y habilitar interrupciones
        LDS #$3BFF
        CLI
        
        ; Inicializacion de estructuras de datos
        MOVB #$FF,Tecla_IN
        MOVB #$FF,Tecla
        MOVB #$00,Cont_Reb
        MOVB #$00,Cont_TCL
        MOVB #$00,Patron
        MOVB #$00,Banderas
        MOVB #50,BRILLO
        MOVB #$0,CONT_TICKS
        MOVB #$0,CONT_DIG
        MOVB #$0,DT
        MOVB #$0,BIN1
        MOVB #$0,BIN2
        MOVB #$0,BCD1
        MOVB #$0,BCD2
        MOVB #$0,BCD_L
        MOVB #$0,DISP1
        MOVB #$0,DISP2
        MOVB #$0,DISP3
        MOVB #$0,DISP4
        MOVB #$0,CANTPQ
        MOVB #$0,AcmPQ
        MOVB #$0,Cuenta
        MOVB VMAX,TIMER_CUENTA

        BSET Banderas,$10          	; CambMod = 1
        MOVB #$FF,1,X+        		; Inicializar el arreglo Num_Array en $FF
        MOVB #$FF,0,X
LOOP_PP:
        TST CantPQ
        BEQ FIRST                       ; Branch si CantPQ = 0
        LDAA PTIH
        ANDA #$80
        LDAB Banderas
        LSLB
        LSLB
        LSLB
        LSLB
        ANDB #$80
        CBA
        BEQ MOD_SELEC                   ; Branch si MODSEL = ModActual
        BSET BANDERAS,$10       	; cambMod=1
MOD_SELEC:
        BRSET PTIH,$80,MOD_CONFIG   	; Branch si MODSEL = 1
MOD_RUN:
        BCLR Banderas,$08           	; Mod Actual = 0
        BRCLR Banderas,$10,FIN_RUN    	; Branch si CambMod = 0
        BCLR Banderas,$10               ; CambMod = 0
        CLR CUENTA
        MOVB #$01,LEDS                  ; Encender PB0
        LDX #Msg_run_L1
        LDY #Msg_run_L2
        JSR Cargar_LCD
FIN_RUN:
        JSR Modo_run
LOOP_PP_INT:
        BRA LOOP_PP
FIRST:
        BSET Banderas,$08               ; ModActual = 1

MOD_CONFIG:
        BSET Banderas,$08               ; ModActual = 1
        BRCLR Banderas,$10,FIN_CONFIG   ; Branch si CambMod = 0
        BCLR Banderas,$10               ; CambMod = 0
        MOVB #$02,LEDS                  ; Encender PB1
        BCLR Banderas,$04               ; Borrar Array_ok
        CLR Cont_TCL
        LDX Num_array
        MOVB #$FF,1,X+
        MOVB #$FF,0,X
        CLR CUENTA
        CLR AcmPQ
        MOVB CANTPQ,BIN1
        CLR CantPQ
        BCLR PORTE,$04                  ; Desactivar Rele
        LDX #Msg_config_L1
        LDY #Msg_config_L2
        JSR Cargar_LCD
FIN_CONFIG:
        JSR MODO_CONFIG
        BRA LOOP_PP_INT

;****************************************************************************
;              MODO RUN
;****************************************************************************

MODO_RUN:
        MOVB #$0F,PIEH
        TST TIMER_CUENTA                ; Se regula la frecuencia con que se suma cuenta
        BNE Ret_RUN
        LDD #VMAX
        STD TIMER_CUENTA
        INC CUENTA
        LDAA CUENTA
        CMPA CantPQ
        BNE Ret_RUN
        INC AcmPQ
        LDAA AcmPQ
        CMPA #100
        BNE no_reset_AcmPQ
        CLR AcmPQ
no_reset_AcmPQ:
        BCLR CRGINT,$80                 ; Detiene interrupcion RTI
        BSET PORTE,$04                  ; Rele
Ret_RUN:
        MOVB AcmPQ,BIN2
        MOVB Cuenta,BIN1
        RTS


;****************************************************************************
;              MODO CONFIG
;****************************************************************************
MODO_CONFIG:
        BSET CRGINT,$80
        CLR AcmPQ
        BCLR PIEH,$03
        JSR TAREA_TECLADO               ; Leer valor de cantPQ
        BRSET Banderas,$04,ARRAY_OK
        BRA ret_config
ARRAY_OK:
        JSR BCD_BIN
        BCLR Banderas,$04
        LDAA CANTPQ
        CMPA #91                        ; Si es mayor que 90 se vuelve a leer otro valor
        BPL array_not_ok
        CMPA #20                        ; Si es mayor que 20 se vuelve a leer otro valor
        BMI array_not_ok
        MOVB CANTPQ,BIN1
        MOVB AcmPQ,BIN2
        BRA array_ok2
Array_not_ok:
        CLR CANTPQ
Array_ok2:
        BCLR Banderas,$04               ; Borrar Array_OK
        CLR Cont_TCL
ret_config:
        RTS

;****************************************************************************
;              BCD_7SEG
;****************************************************************************
BCD_7SEG:
        LDX #SEGMENT
        LDAA BCD1                  ; BCD1
        ANDA #$0F                  ; Parte baja
        MOVB A,X,DISP4
        LDAA BCD1
        ANDA #$F0                  ; Parte alta
        CMPA #$B0
        BNE DISP3_B
        MOVB #$B,DISP3
        BRA END_DISP3
DISP3_B:
        LSRA
        LSRA
        LSRA
        LSRA
        MOVB A,X,DISP3
END_DISP3:
        LDAA BCD2                ; BCD2
        ANDA #$0F                ; Parte baja
        MOVB A,X,DISP2
        LDAA BCD2
        ANDA #$F0                ; Parte alta
        CMPA #$B0
        BNE DISP1_B
        MOVB #$B,DISP1
        BRA END_DISP1
DISP1_B:
        LSRA
        LSRA
        LSRA
        LSRA
        MOVB A,X,DISP1
END_DISP1:
        RTS


;****************************************************************************
;              CONV_BIN_BCD
;****************************************************************************
CONV_BIN_BCD:
        LDAA BIN1
        JSR BIN_BCD
        LDAA BCD_L
        ANDA #$F0           ; Se revisa si hay cero a la izquierda
        BNE No_es_cero1
        LDAA BCD_L
        ADDA #$B0
        STAA BCD_L
No_es_cero1:
        MOVB BCD_L,BCD1
        LDAA BIN2
        JSR BIN_BCD
        LDAA BCD_L
        ANDA #$F0              ; Se revisa si hay cero a la izquierda
        BNE No_es_cero2
        LDAA BCD_L
        ADDA #$B0
        STAA BCD_L
No_es_cero2:
        MOVB BCD_L,BCD2
        RTS
        
;****************************************************************************
;              BIN BCD
;****************************************************************************

BIN_BCD:
        CLR BCD_L
        LDY #7
LOOP_BIN_BCD:
        LSLA              	; Se hace un desplazamiento en el numero binario
        ROL BCD_L         	; Se desplaza la parte menos significativa
        TFR A,X                 ; Se respalda el numero en binario desplazado en X
        LDAA BCD_L
        ANDA #$0F               ; Se carga en B la parte baja del numero en BCD
        CMPA #$5         	; Si es menor a 5 el valor se mantiene
        BMI MENOR
        ADDA #$3            	; Se le suma 3 en caso de ser mayor o igual a 5
MENOR:
        LDAB BCD_L
        ANDB #$F0        	; Se carga en B la parte alta del numero en BCD
        CMPB #$50         	; Si esa parte es menor que 5 el valor se mantiene
        BMI MENOR2
        ADDB #$30       	; Si es mayor o igual, se le suma 3
MENOR2:
        ABA                	; Se suman la parte mas significativa y la menos significativa del numero
        STAA BCD_L
        TFR X,A
        DBNE Y,LOOP_BIN_BCD  	; En el caso de haber desplazado 7 bits se sale del loop
        LSLA          		; Se hace el desplazamiento final en el numero binario;
        ROL BCD_L       	; Se hace el desplazamiento final en el numero en BCD
        RTS


;****************************************************************************
;              BCD BIN
;****************************************************************************

BCD_BIN:

        LDD Num_Array               ; Se carga en D el numero en BCD
        CPD #$FFFF
        BEQ RET_BCD_BIN
        ANDB #$0F               ; Se carga en B la parte baja del byte menos significativo
        STAB CANTPQ             ; Se guarda en NUM_BIN el resultado de procesar los primeros 4 bits
        LDD Num_Array
        ANDA #$0F               ; Se carga en B la parte baja del byte menos significativo
        LDAB #10
        MUL
        LDAA CANTPQ
        ABA
        STAA CANTPQ             ; Se multiplican los bits seleccionados por la potencia de 10 correspondiente
RET_BCD_BIN:
        RTS



;****************************************************************************
;              CARGAR LCD
;****************************************************************************
Cargar_LCD:
        PSHX
        LDX #iniDsp
        LDAB 1,X+
LOOP_LCD:
        LDAA 1,X+
        JSR SEND_COMMAND            ; iniDsp
        MOVB D40uS,Cont_Delay
        JSR Delay               	; 40us
        DBNE B, LOOP_LCD
        LDAA clear_LCD
        JSR SEND_COMMAND        	; clr_LCD
        MOVB D2mS,Cont_Delay
        JSR Delay                  	; 2ms
        PULX
        LDAA ADD_L1
        JSR SEND_COMMAND           	; L1
        MOVB D40uS,Cont_Delay
        JSR DELAY                   	; 40us
Loop_cargar:
        LDAA 1,X+
        CMPA #EOM
        BEQ Cont3
        JSR SEND_DATA                   ; msgL1
        MOVB D40uS,Cont_Delay
        JSR Delay                  	; 40us
        BRA Loop_cargar

Cont3:  LDAA ADD_L2
        JSR SEND_COMMAND            	; L2
        MOVB D40uS,Cont_Delay
        JSR Delay                  	; 40 us
Loop2_cargar:
        LDAA 1,Y+
        CMPA #EOM
        BEQ Return_Cargar
        JSR SEND_DATA                	; MSG L2
        MOVB D40uS,Cont_Delay
        JSR Delay                   	; 40us
        BRA Loop2_Cargar
Return_Cargar:
        RTS


;****************************************************************************
;              SEND DATA
;****************************************************************************
SEND_DATA:
        PSHA
        ANDA #$F0
        LSRA
        LSRA
        STAA PORTK
        BSET PORTK,$01          ; RS=1
        BSET PORTK,$02          ; EN=1
        MOVB D260uS,Cont_Delay
        JSR Delay
        BCLR PORTK,$02          ; EN=0
        PULA
        ANDA #$0F
        LSLA
        LSLA
        STAA PORTK
        BSET PORTK,$01          ; RS=1
        BSET PORTK,$02          ; EN=1
        MOVB D260uS,Cont_Delay
        JSR Delay
        BCLR PORTK,$02          ; EN=0
        RTS


;****************************************************************************
;              SEND COMMAND
;****************************************************************************
SEND_COMMAND:
        PSHA
        ANDA #$F0
        LSRA
        LSRA
        STAA PORTK
        BCLR PORTK,$01          ; RS=0
        BSET PORTK,$02          ; EN=1
        MOVB D260uS,Cont_Delay
        JSR Delay
        BCLR PORTK,$02          ; EN=0
        PULA
        ANDA #$0F
        LSLA
        LSLA
        STAA PORTK
        BCLR PORTK,$01          ; RS=0
        BSET PORTK,$02          ; EN=1
        MOVB D260uS,Cont_Delay
        JSR Delay
        BCLR PORTK,$02          ; EN=0
        RTS


;****************************************************************************
;              DELAY
;****************************************************************************

Delay:
        TST Cont_Delay
        BNE Delay
        RTS


;****************************************************************************
;              TAREA_TECLADO
;****************************************************************************

TAREA_TECLADO:
        TST Cont_Reb
        BNE Retornar                    ; No han terminado los rebotes
        JSR MUX_TECLADO
        BRSET Tecla,$FF,NoPres          ; No se ha presionado una tecla
        BRCLR Banderas,$02,NoProcesada  ; No se ha procesado tecla
        LDAA Tecla
        CMPA Tecla_IN
        BEQ Lista                       ; Valor de tecla valido
        MOVB #$FF,Tecla
        MOVB #$FF,Tecla_IN
        BCLR Banderas,$02               ; Se limpia bandera TCL_Leida
        BCLR Banderas,$01               ; Se limpia bandera TCL_Lista
        BRA RETORNAR
NoPres:                                 ; No se ha presionado tecla
        BRCLR Banderas,$01,RETORNAR     ; Si no se tiene ninguna tecla lista se retorna
        BCLR Banderas,$01
        BCLR Banderas,$02
        JSR FORMAR_ARRAY                ; Si hay tecla lista, se llama a subrutina FORMAR_ARRAY
        BRA RETORNAR
NoProcesada:                            ; Si todavia no se ha procesado la tecla
        MOVB Tecla,Tecla_IN
        BSET Banderas,$02
        MOVB 10,CONT_REB
        BRA RETORNAR
Lista:
        BSET Banderas,$01

RETORNAR:
        RTS

;****************************************************************************
;              MUX_TECLADO
;****************************************************************************
MUX_TECLADO:
        LDX #Teclas
        MOVB #$EF,Patron                  ; Primer patron
        CLRB
LOOP_MUX:
        MOVB Patron,PORTA                 ; Se pone el patron en el puerto A
        NOP
        NOP
        NOP
        NOP
        NOP
        BRCLR PORTA,$01,FOUND              ; Se revisa si fue presionada una tecla de la primera columna
        INCB
        BRCLR PORTA,$02,FOUND              ; Se revisa si fue presionada una tecla de la segunda columna
        INCB
        BRCLR PORTA,$04,FOUND              ; Se revisa si fue presionada una tecla de la tercera columna
        INCB
        LSL Patron                         ; Se desplaza a la izquierda para obtener el nuevo patron
        BRSET Patron,$F0,BORRAR
        BRA LOOP_MUX
FOUND:
        MOVB B,X,Tecla                     ; Se guarda el valor de la tecla
        RTS
BORRAR:
        MOVB #$FF,Tecla                    ; Se borra la tecla
        RTS


;****************************************************************************
;              FORMAR_ARRAY
;****************************************************************************

FORMAR_ARRAY:
        LDY #Num_Array
        LDAA Tecla_IN
        LDAB CONT_TCL
        CMPB MAX_TCL                     ; Se revisa si ya se lleno el arreglo
        BEQ TCL_BORRAR                  ; En caso de estar lleno hace branch
        CMPB #0
        BEQ TCL_BORRAR_ENTER            ; Hace branch si es la primera tecla
        CMPA #$0B
        BEQ REGRESAR_PTR                ; Hace branch si la tecla es $0B
        CMPA #$0E
        BEQ LISTO                       ; Hace branch si la tecla es $0E
VALOR_ARREGLO:
        MOVB Tecla_IN,B,Y               ; Se guarda el valor de la tecla en el arreglo
        INCB
        STAB CONT_TCL
BORRAR_IN:
        MOVB #$FF,TECLA_IN              ; Se borra el valor de tecla_in
        RTS                             ; Se retorna
TCL_BORRAR:
        CMPA #$0B                       ; Si es la tecla borrar, se borra la ultima posicion
        BEQ REGRESAR_PTR
        CMPA #$0E                       ; Si es la tecla enter, se da por terminado el arreglo
        BNE BORRAR_IN
LISTO:
        BSET Banderas,$4                ; Array_OK = 1
        BRA BORRAR_IN
TCL_BORRAR_ENTER:                       ; En el caso de que sea la primera tecla
        CMPA #$0B
        BEQ BORRAR_IN                   ; Si es borrar se ignora
        CMPA #$0E
        BEQ BORRAR_IN                   ; Si es enter se ignora
        BRA VALOR_ARREGLO               ; En caso de no ser ni enter ni borrar, se escribe el valor en el arreglo
REGRESAR_PTR:
        DECB
        MOVB #$FF,B,Y                   ; Se borra el valor del arreglo
        STAB Cont_TCL
        BRA BORRAR_IN




;****************************************************************************
;              SUBRUTINAS DE SERVICIO DE INTERRUPCIONES
;****************************************************************************

;****************************************************************************
;              OC4_ISR
;****************************************************************************
OC4_ISR:
        LDAA #100                    ; Se calcula el valor de DT
        LDAB BRILLO
        SBA
        STAA DT
        LDAA CONT_TICKS
        CMPA #100
        BNE CONT_OC4_int        ; Hace branch en caso de no haber alcanzado los 100 ticks
        BSET PTJ,$02
        CLR CONT_TICKS          ; Se reinicia el contador de ticks
        TST CONT_DIG
        BNE DIG1                ; DISP 1
        INC CONT_DIG
        LDAA DISP1
        CMPA #$B
        BEQ NO_DISP3
        MOVB DISP1,PORTB
        MOVB #$0E,PTP
        BRA CONT_OC4_INT

DIG1:                            ; DISP 2
        LDAA CONT_DIG
        CMPA #1
        BNE DIG2
        INC CONT_DIG
        BRSET Banderas,$08,NO_DISP3
        MOVB DISP2,PORTB
        MOVB #$0D,PTP
        BRA CONT_OC4

CONT_OC4_INT:
        BRA CONT_OC4
DIG2:                           ; DISP 3
        LDAA CONT_DIG
        CMPA #2
        BNE DIG3
        INC CONT_DIG
        LDAA DISP3
        CMPA #$B
        BEQ NO_DISP3
        MOVB DISP3,PORTB
        MOVB #$0B,PTP
        BRA CONT_OC4
NO_DISP3:                       ; En caso de 0 a la izquierda
        MOVB #0,PORTB
        BRA CONT_OC4
DIG3:                           ; DISP 4
        LDAA CONT_DIG
        CMPA #3
        BNE ACT_LEDS
        MOVB DISP4,PORTB
        MOVB #$07,PTP
        INC CONT_DIG
        BRA CONT_OC4
ACT_LEDS:                   	; Encender led correspondiente al modo
        BCLR PTJ,$02
        MOVB LEDS,PORTB
        CLR CONT_DIG
CONT_OC4:
        LDAA DT
        LDAB CONT_TICKS
        CBA
        BNE FIN_DT
        MOVB #$FF,PTP
        BSET PTJ,$02
FIN_DT:
        INC CONT_TICKS
        LDD CONT_7SEG
        CPD #$0                   ; Actualizar valor a desplegar en los 7 segmentos
        BNE NO_REFRESH
        MOVW #5000,CONT_7SEG
        JSR CONV_BIN_BCD
        JSR BCD_7SEG
        
NO_REFRESH:
        LDX CONT_7SEG
        DEX
        STX CONT_7SEG
        TST Cont_Delay
        BEQ Ret
        DEC Cont_Delay
Ret:    LDD TCNT
        ADDD #60
        STD TC4
        RTI


;****************************************************************************
;              RTI_ISR
;****************************************************************************

RTI_ISR:
        BSET CRGFLG,$80         ; Borrar bandera
        LDX TIMER_CUENTA
        BEQ testReb
        DEX
        STX TIMER_CUENTA

testReb:
        TST Cont_REB
        BEQ Return_RTI
        DEC Cont_REB
Return_RTI:
        RTI

;****************************************************************************
;              PTH_ISR
;****************************************************************************


PTH_ISR:
        TST CONT_REB           ; Supresion de rebotes
        BNE RET_PTH
        BRSET PIFH,$01,PTH0
        BRSET PIFH,$02,PTH1
        BRSET PIFH,$04,PTH2
        BRSET PIFH,$08,PTH3
PTH0:
        MOVB #$01,PIFH          ; Se baja la bandera
        BCLR PORTE,$04          ; Rele
        CLR Cuenta
        BSET CRGINT,$80         ; Se habilitan interrupciones rti
        BRA RET_PTH
PTH1:
        MOVB #$02,PIFH          ; Se baja la bandera
        CLR AcmPQ
        BRA RET_PTH
PTH2:
        MOVB #$04,PIFH          ; Se baja la bandera
        LDAA BRILLO
        CMPA #100
        BEQ RET_PTH
        ADDA #5
        STAA BRILLO
        BRA RET_PTH
PTH3:
        MOVB #$08,PIFH          ; Se baja la bandera
        LDAA BRILLO
        CMPA #5
        BEQ RET_PTH
        SUBA #5
        STAA BRILLO
        BRA RET_PTH
RET_PTH:
        RTI