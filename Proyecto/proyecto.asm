;***********************************************************************************************************************************
; Escuela de Ingenieria Electrica
; Curso Microprocesadores
; IE 0623
; Trabajo Final
; Selector 623
; Natalia Bolanos Murillo B61127
; Ultima vez modificado 12/07/20
;***********************************************************************************************************************************
;
;***********************************************************************************************************************************
#include registers.inc
;***********************************************************************************************************************************
; Descripcion General
; El siguiente codigo corresponde a un selector de tubos que demarca los que cumplen con la longitud minima indicada por el usuario.
; El selector cuenta con tres modos, el primero permite ingresar el valor deseado como longitud minima, esto se hace por medio del
; teclado matricial y si el valor ingresado es valido se muestra en los displays de 7 segmentos. El segundo modo se encarga de
; calcular la velocidad y la longitud del tubo sensado por S1 y S2, estos se simulan con los botones PH3 y PH0 respectivamente.
; El boton de S1 se debe mantener presionado siempre que este sensando el tubo y el de S2 se presiona cuando sense el tubo por primera
; vez. El valor de la velocidad y de la longitud se muestra los displays de 7 segmentos y en la pantalla LCD se muestra si la
; longitud es correcta o deficiente, en el caso de ser correcta se activa el rele (rociador). Por ultimo, en el tercer modo no se
; hacen mediciones y se mantienen apagados los displays de 7 segmentos.
; Los modos son eleccionados con PH7 y PH6 de la siguiente manera:
; PH7-PH6: OFF-OFF: MODO STOP
; PH7-PH6: ON-OFF:  MODO CONFIG
; PH7-PH6: ON-ON:   MODO SELECT
;
;
;***********************************************************************************************************************************
;                                       DECLARACION DE ESTRCUTURAS DE DATOS
;***********************************************************************************************************************************
                org $1000
                
BANDERAS:       ds 2 ; Variable de 1 word utilizada para guardar las banderas
                     ; La parte alta se accede como Banderas:
                     ;                                          Bit 15: Display_calc
                     ; La parte baja se accede como Banderas+1:
                     ;                                          Bit 7: Mod_Actual
                     ;                                          Bit 6: Mod_Actual
                     ;                                          Bit 5: Camb_Mode
                     ;                                          Bit 4: Calc_ticks
                     ;                                          Bit 3: Pant_Flg
                     ;                                          Bit 2: Array_OK
                     ;                                          Bit 1: TCL_Leida
                     ;                                          Bit 0: TCL_Lista
                     
; Config
LengthOK:       ds 1 ; Variable utiliada para guardar el valor ingresado por el usuario
ValorLength:    ds 1 ; Variable utilizada para respaldar el valor de LengthOK

; Tarea_Teclado
MAX_TCL:        db 2 ; Constante de 1 byte que indica el tamano maximo del arreglo Num_Array
Tecla:          ds 1 ; Variable de 1 byte utilizada para almacenar el valor a guardar en el arreglo
Tecla_IN        ds 1 ; Variable de 1 byte donde la subrutina FORMAR_ARRAY lee el valor de la tecla presionada
Cont_Reb:       ds 1 ; Variable de 1 byte utilizada como contador de rebotes
Cont_TCL:       ds 1 ; Variable de 1 byte utilizada para almacenar el numero de tecla en Num_Array
Patron:         ds 1 ; Variable de 1 byte utilizada para cargar distintos patrones en el puerto A
Num_Array:      ds 2 ; Arreglo de 6 bytes donde se guardan los valores ingresados

; ATD_ISR
BRILLO:         ds 1 ; Variable de 1 byte que controla el brillo de la pantalla de 7 segmentos
POT:            ds 1 ; Variable utilizada para guardar el promedio de las 5 lecturas del potenciometro

; Pant_ctrl
TICK_EN:        ds 2 ; Cantidad de ticks necesarios para que el centro longitudinal se encuentre frente al rociador
TICK_DIS:       ds 2 ; Cantidad de ticks necesarios para que el tubo supere la posicion del rociador
CONT_ROC:       ds 1 ; Cantidad de ticks necesario para que pasen 0.2 segundos

; CALCULAR
VELOC:          ds 1 ; Variable que almacena el valor de la velocidad del tubo
LONG:           ds 1 ; Variable que almacena el valor de la longitud del tubo

; TCNT_ISR
TICK_MED:       ds 2 ; Variable utilizada medir la cantidad de ticks que le toma al tubo pasar de un sensor a otro y el tiempo que
                     ; le toma superar la posicion de S1, esto para calcular la velocidad y la longitud

; Conv_BIN_BCD
BIN1:           ds 1 ; Variable que guarda el numero en bianrio que se debe desplegar en DISP3 y DISP4 de la pantalla de 7 segmentos
BIN2:           ds 1 ; Variable que guarda el numero en bianrio que se debe desplegar en DISP1 y DISP2 de la pantalla de 7 segmentos
BCD1:           ds 1 ; Variable donde la subrutina Conv_BIN_BCD guarda el resultado de la conversion de BIN1 a BCD
BCD2:           ds 1 ; Variable donde la subrutina Conv_BIN_BCD guarda el resultado de la conversion de BIN2 a BCD

; BIN_BCD
BCD_L:          ds 1 ; Variable donde la subrutina BIN_BCD guarda el resultado en BCD
BCD_H:          ds 1 ; Variable no se requiere pues el número a convertir es menor que 99.
TEMP:           ds 1 ; Variable utilizada para respaldar valor en Bin_Bcd
LOW:            ds 1 ; Variable utilizada para respaldar valor en Bin_Bcd

; BCD_7SEG
DISP1:          ds 1 ; Variable que guarda el valor a desplegar en el display 1 de la pantalla de 7 segmentos
DISP2:          ds 1 ; Variable que guarda el valor a desplegar en el display 2 de la pantalla de 7 segmentos.
DISP3:          ds 1 ; Variable que guarda el valor a desplegar en el display 3 de la pantalla de 7 segmentos
DISP4:          ds 1 ; Variable que guarda el valor a desplegar en el display 4 de la pantalla de 7 segmentos

; OC4_ISR
LEDS:           ds 1 ; Variable que guarda el estado de los LEDs a ser desplegado
CONT_DIG:       ds 1 ; Variable utilizado para la multiplexacion de la pantalla de 7 segmentos
CONT_TICKS:     ds 1 ; Variable utilizada para la multiplexacion de la pantalla de 7 segmentos
DT:             ds 1 ; Variable que maneja el ciclo de trabajo de encendido para los d\'igitos y los LEDs
CONT_7SEG:      ds 2 ; Varibale utilizada para el refrescamiento de los valores a desplegar en la pantalla de 7 segmentos
CONT_200:       ds 1 ; Variable utilizada para hacer conversion ATD cada 200 ms

; Subrutinas LCD
Cont_delay:     ds 1 ; Variable utilizada como contador para realizar los delays
D2mS:           db 100 ; Constante de 1 byte utilizada para el retardo de 2 ms
D260uS:         db 13  ; Constante de 1 byte utilizada para el retardo de 260 us
D40uS:          db 2   ; Constante de 1 byte utilizada para el retardo de 40 us
Clear_LCD:      db $01 ; Constante de 1 byte con comando para borrar la LCD
ADD_L1:         db $80 ; constante igual a Adress linea 1 lcd
ADD_L2:         db $C0 ; constante igual a Adress linea 2 lcd

V_temp:         ds 1 ; Variable temporal utilizada en el calculo de tick_en
Veloc_temp:     ds 1 ; Variable para guardar el valor de VELOC temporalmente mientras se obtiene el valor de LONG

                org $1040
                
Teclas:         db $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E ; Tabla con los valores de las teclas

                org $1050
                
Segment:        db $3F,$06,$5B,$4F,$66,$6D,$7D,$07,$7F,$6F,$40,$00 ; Tabla para conversion BCD a 7 segmentos

                org $1060
                
iniDsp:         db $04,$28,$28,$06,$0C ; Tabla de comandos para iniciar la comunicacion con la pantalla LCD

; Mensajes para la pantalla LCD
                org $1070
EOM:            equ $0
Msg_config_L1:          fcc " CONFIGURACION"
                db EOM
Msg_config_L2:          fcc "    LengthOK"
                db EOM
Msg_inicial_L1:          fcc "  MODO SELECT"
                db EOM
Msg_inicial_L2:          fcc "  Esperando..."
                db EOM
Msg_longc_L1:          fcc "   *LONGITUD*"
                db EOM
Msg_longc_L2:          fcc "   *CORRECTA*"
                db EOM
Msg_longd_L1:          fcc "   -LONGITUD-"
                db EOM
Msg_longd_L2:          fcc "  -DEFICIENTE-"
                db EOM
Msg_alerta_L1:          fcc "   VELOCIDAD"
                db EOM
Msg_alerta_L2:          fcc "FUERA DE RANGO"
                db EOM
Msg_BIENV_L1:          fcc "    SELECTOR"
                db EOM
Msg_BIENV_L2:          fcc "      623"
                db EOM
Msg_espera_L1:          fcc "  MODO SELECT"
                db EOM
Msg_espera_L2:          fcc " Calculando..."
                db EOM

;***********************************************************************************************************************************
;                                          RELOCALIZACION DE VECTORES DE INTERRUPCION
;***********************************************************************************************************************************
                org $3E70
                dw RTI_ISR
                org $3E4C
                dw CALCULAR
                org $3E66
                dw OC4_ISR
                org $3E5E
                dw TCNT_ISR
                org $3E52
                dw ATD_ISR
;***********************************************************************************************************************************
;***********************************************************************************************************************************
;                  `                           PROGRAMA PRINCIPAL
;***********************************************************************************************************************************
;***********************************************************************************************************************************

                org $2000
;***********************************************************************************************************************************
;                                              CONFIGURACION DE HARDWARE
;***********************************************************************************************************************************
        ; Puerto E
        BSET DDRE,$04    	; Rele

        ; Puerto H
        MOVB #$0,PPSH       	; Flanco negativo

        ; RTI
        MOVB #$23,RTICTL     	; Se carga base de tiempo para RTI

        ; ATD
        MOVB #$28,ATD0CTL3    	; 5 mediciones
        MOVB #$97,ATD0CTL4      ; 8 bits, 2 ciclos, PRS 23
        MOVB #$87,ATD0CTL5

        ; Teclado
        MOVB #$F0,DDRA      	; Parte alta es salida, parte baja es entrada
        BSET PUCR,$01        	; Pullup

        ; LEDS
        MOVB #$FF,DDRB          ; Se habilita puerto B como salida
        BSET DDRJ,$02
        BSET PTJ, $02

        ; 7 SEG
        MOVB #$0F,DDRP
        MOVB #$0F,PTP

        ; OC4
        MOVB #$80,TSCR1
        MOVB #$03,TSCR2         ; Preescalador 8
        MOVB #$10,TIOS

        MOVB #$00, TCTL1
        MOVB #$00, TCTL2
       ; LCD
        MOVB #$FF,DDRK
        


        ; Definir puntero de pila y habilitar interrupciones
        LDS #$3BFF
        CLI

        ; Inicializacion de estructuras de datos
        MOVB #$FF,Tecla_IN
        MOVB #$FF,Tecla
        MOVB #$00,Cont_Reb
        MOVB #$00,Cont_TCL
        MOVB #$00,Patron
        MOVW #$00,Banderas
        MOVB #50,BRILLO
        MOVB #$0,CONT_TICKS
        MOVB #$0,CONT_DIG
        MOVB #$0,DT
        MOVB #$BB,BIN1
        MOVB #$BB,BIN2
        MOVB #$0,BCD1
        MOVB #$0,BCD2
        MOVB #$0,BCD_L
        MOVB #$0,DISP1
        MOVB #$0,DISP2
        MOVB #$0,DISP3
        MOVB #$0,DISP4
        MOVB #$00,LengthOK
        MOVB #$0,ValorLength
        MOVB #$0,VELOC
        MOVB #$0,LONG
        LDD TCNT
        ADDD #60
        STD TC4
        BSET Banderas+1,$20          	; CambMod = 1
        BSET Banderas+1,$80           	; ModActual = config
        MOVW #$FFFF,Num_array          	; Inicializar el arreglo Num_Array en $FF
        MOVB #$10,TIE                   ; Se habilitan interrupciones OC4
        BSET CRGINT,$80                 ; Se habilitan interrupciones rti
        MOVB #$C2,ATD0CTL2
        LDAA #200
loop_ATD:
        DBNE A,loop_ATD         ; Retardo para encender el convertidor
        
;***********************************************************************************************************************************
;                                                       MAIN
;***********************************************************************************************************************************
; Descripcion
; Programa principal encargado de detectar cual es el modo seleccionado, esto lo hace por medio
; de los dipswitch PH7 y PH6. Llama a las subrutinas respectivas dependiendo del modo seleccionado.
; Parametros de Entrada
;       LengthOK: Valor ingresado por el usuario, no se puede cambiar de modo hasta que su valor sea distinto de 0
;       Msg_config_L1: Mensaje para desplegar en la LCD
;       Msg_config_L2: Mensaje para desplegar en la LCD
;       Msg_inicial_L1: Mensaje para desplegar en la LCD
;       Msg_inicial_L2: Mensaje para desplegar en la LCD
;       Msg_BIENV_L1: Mensaje para desplegar en la LCD
;       Msg_BIENV_L2: Mensaje para desplegar en la LCD
; Parametros de Salida
;       BIN1: Se ponen en $BB cuando se ingresa al modo select
;       BIN2: Se ponen en $BB cuando se ingresa al modo select
;       LEDS: Se enciende el LED correspondiente a cada modo
;***********************************************************************************************************************************

        LDX #Msg_config_L1        	; Se pone mensaje de configuracion en LCD
        LDY #Msg_config_L2
        JSR Cargar_LCD                  ;
        MOVB #$01,LEDS                  ; Encender Led de CONFIG

loop_config:
        JSR CONFIG                      ; Se llama subrutina CONFIG hasta que se guarde un valor en LengthOK
        LDAA LengthOK
        BEQ loop_config
loop_pp:
        LDAA Banderas+1              	; Se revisa si hay cambio de modo
        ANDA #$C0
        LDAB PTIH
        ANDB #$C0
        CBA
        BEQ Cont_main                   ; Si no hay cambio de modo se deja bandera CambMod = 0
        BSET Banderas+1,$20         	; Bandera CambMod=1
Cont_main:
        CMPB #$C0                       ; Se revisan solo los bits de modo
        BNE NO_SELECT                	; Se revisa si es modo select
        BSET TSCR2,$80                  ; Se habilitan interrupciones TOI
        MOVB #$09,PIEH                  ; Habilita interrupciones PTH
        BRCLR Banderas+1,$20,select_cm 	; En caso de cambio de modo se actualiza LCD
        BCLR Banderas+1,$20
        LDX #Msg_inicial_L1         	; Se pone mensaje inicial en LCD
        LDY #Msg_inicial_L2
        JSR Cargar_LCD
        MOVB #$02,LEDS                  ; Encender Led de Select
        BSET Banderas+1,$C0             ; ModActual=select
        MOVB #$BB,BIN1                  ; Se apagan los display de 7 segmentos
        MOVB #$BB,BIN2
select_cm:
        JSR SELECT                      ; Modo Select
loop_pp_int:
        BRA loop_pp
NO_SELECT:
        CLR VELOC
        CLR LONG
        MOVB #$00,PIEH                  ; Deshabilita interrupciones PTH
        BCLR TSCR2,$80                  ; Se habilitan interrupciones TOI
        LDAA PTIH
        ANDA #$C0                    	; Se utilizan solo los bits de modo
        CMPA #$80                  	; Se revisa si el como config
        BEQ M_CONFIG
        CMPA #$0
        BNE loop_pp
        BRCLR Banderas+1,$20,stop_cm  	; En caso de cambio de modo se actualiza LCD
        BCLR Banderas+1,$20
        LDX #Msg_BIENV_L1           	; Se pone mensaje inicial en LCD
        LDY #Msg_BIENV_L2
        JSR Cargar_LCD
        MOVB #$04,LEDS                  ; Encender Led de Stop
        BCLR Banderas+1,$C0             ; ModActual=stop
stop_cm:
        JSR STOP
        BRA loop_pp_int
M_CONFIG:
        BRCLR Banderas+1,$20,config_cm  ; En caso de cambio de modo se actualiza LCD
        BCLR Banderas+1,$20
        LDX #Msg_config_L1        	; Se pone mensaje inicial en LCD
        LDY #Msg_config_L2
        JSR Cargar_LCD
        MOVB #$01,LEDS                  ; Encender Led de CONFIG
        BSET Banderas+1,$80             ; ModActual=config
        BCLR Banderas+1,$40
config_cm:
        JSR CONFIG                      ; Llamado subrutina CONFIG
RET_MAIN:
        BRA loop_pp_int
        
;***********************************************************************************************************************************
;***********************************************************************************************************************************
;                                        SUBRUTINAS DE SERVICIO DE INTERRUPCIONES
;***********************************************************************************************************************************
;***********************************************************************************************************************************

;***********************************************************************************************************************************
;                                                       OC4_ISR
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de recibir el valor de los numeros a ser desplegados en los displays de 7 segmentos
; en las variables DISP1, DISP2, DISP3 y DISP4, asi como la varible LEDS, estos son desplegados en la pantalla
; y el puerto de leds de manera multiplexada con una frecuencia de 100 Hz/Digito-Leds.
; Tambien se encarga de actualizar los valores a desplegar en los display de 7 segmentos cada 100 ms, esto lo
; hace llamando a las subrutinas CONV_BIN_BCD y BCD_7SEG.
; Por otra parte, se encarga de decrementar Cont_Delay asi como de incrementar CONT_TICKS y CONT_DIG.
;
; Parametros de Entrada
;       DISP1, DISP2, DISP3, DISP4: Se reciben por memoria, contienen los valores a desplegar en los displays de 7 segmentos.
;       BRILLO: Esta variable se recibe por memoria. Se utiliza para calcular el valor de DT, lo cual controla el brillo
;               de los displays de 7 segmentos.
;       CONT_DELAY: Variable se recibe por memoria. Se decrementa en 1 si su valor es distinto de 0.
;       LEDS: Variable recibida por memoria. Contiene el patron que se debe cargar en el Puerto B para encender los leds.
;
; Parametros de Salida
;       CONT_DELAY: Variable se devuelve por memoria. Es utilizada por otras subrutinas para generar delays.
;***********************************************************************************************************************************
OC4_ISR:
        BSET TFLG1,$10      	; Se baja bandera
        LDD TCNT                ; Se carga TCNT para sumarle el periodo de interrupcion
        ADDD #60
        STD TC4                 ; TC4=TCNT+60
        LDD CONT_7SEG
        CPD #5000
        BNE NO_REFRESH          ; Se revisa si es momento de actualizar los valores de los 7 segmentos
        JSR CONV_BIN_BCD        ; Si CONT_7SEG es 5000 entonces se actualizan los valores de los 7 segmentos
        CLR CONT_7SEG
        JSR BCD_7SEG
        BRA CONT_OC4
NO_REFRESH:                     ; Si CONT_7SEG no es 5000, se le suma 1
        ADDD #1
        STD CONT_7SEG
CONT_OC4:
        LDAA BRILLO
        BEQ BRILLO_0            ; Si BRILLO=0, se apagan los displays
        TST CONT_TICKS
        BNE TST_DT
        TST CONT_DIG            ; Si CONT_TICKS=0 se procede a cargar los valores en cada display
        BNE DIG1                ; DISPLAY 1
        BSET PTJ,$02
        MOVB #$0E,PTP           ; Se selecciona el display 1
        MOVB disp1,PORTB        ; Se carga el valor respectivo en el puerto B
        BRA CONTADORES_int
DIG1:                        	; DISPLAY 2
        LDAA CONT_DIG
        CMPA #1
        BNE DIG2
        BSET PTJ,$02
        MOVB #$0D,PTP           ; Se selecciona el display 2
        MOVB disp2,PORTB        ; Se carga el valor respectivo en el puerto B
        BRA CONTADORES
DIG2:                           ; DISPLAY 3
        CMPA #2
        BNE DIG3
        BSET PTJ,$02
        MOVB #$0B,PTP           ; Se selecciona el display 3

        MOVB disp3,PORTB        ; Se carga el valor respectivo en el puerto B
        BRA CONTADORES
DIG3:                           ;DISPLAY 4
        CMPA #3
        BNE CONT_LEDS
        BSET PTJ,$02
        MOVB #$07,PTP           ; Se selecciona el display 4
        MOVB disp4,PORTB        ; Se carga el valor respectivo en el puerto B
CONTADORES_INT:
        BRA CONTADORES
CONT_LEDS:                      ; LEDS
        MOVB #$0F,PTP           ; Se seleccionan los leds
        BCLR PTJ,$02            ; Se encienden los leds
        MOVB LEDS,PORTB         ; Se carga el valor respectivo en el puerto B
        BRA CONTADORES
TST_DT:
        LDAA CONT_TICKS         ; En el caso de que CONT_TICKS != 0 entonces se compara con DT
        CMPA DT
        BNE POS_BRILLO_0
BRILLO_0:                       ; Si BRILLO=0 o CONT_TICKS=DT, se apagan los displays
        MOVB #$00,PORTB
        MOVB #$0F,PTP
POS_BRILLO_0:
        LDAA CONT_TICKS
        CMPA #100
        BNE CONTADORES
        LDAA #20                ; Si CONT_TICKS=100, se calcula DT
        SUBA BRILLO             ; 20-Brillo
        LDAB #5
        MUL                     ; D=(20-Brillo)*5
        LDAA #100
        SBA
        STAA DT                 ; DT = 100 - (20-Brillo)*5
        INC CONT_DIG            ; Se incrementa CONT_DIG para pasar al siguiente digito
        CLR CONT_TICKS          ; Se reinicia CONT_TICKS
        LDAA CONT_DIG
        CMPA #5
        BNE pos_contadores
        CLR CONT_DIG            ; Si CONT_DIG=5, se reinicia
        BRA pos_contadores
CONTADORES:
        INC CONT_TICKS
pos_contadores:
        TST CONT_DELAY         ; Se decrementa CONT_DELAY en 1 siempre que sea distinto de 0
        BEQ ret_oc4
        DEC CONT_DELAY
ret_oc4:
        RTI

;***********************************************************************************************************************************
;                                                       RTI_ISR
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de manejar la supresion a rebotes del teclado matricial y de los botones PH0 y PH3.
; Ademas se encarga de dar inicio a las conversiones ATD al escribir en ATD0CTL5 cada 200 ms y tambien controla el tiempo en que
; el rociador se encuentra activo, esto por medio de la variable CONT_ROC.
;
; Parametros de Entrada
;       CONT_REB: Variable recibida por memoria el valor actual del contador.
;       CONT_ROC: Variable se recibe por memoria el valor actual del contador.
;
; Parametros de Salida
;       CONT_REB: Se devuelve por memoria el valor decrementado en 1 o en cero.
;       CONT_ROC: Se devuelve por memoria el valor decrementado en 1 o en cero.
;***********************************************************************************************************************************

RTI_ISR:
        BSET CRGFLG,$80         ; Borrar bandera
        LDAA CONT_200
        BEQ En_ATD
        DECA                 	; CONT_200 se decrementa en 1 si su valor es distinto de cero
        STAA CONT_200
        BRA tstCont_roc
En_ATD:
        MOVB #200,CONT_200      ; Si es igual a cero, se reinicia su valor
        MOVB #$87,ATD0CTL5      ; Se inicia la conversion ATD

tstCont_roc:
        TST CONT_ROC            ; CONT_ROC se decrementa en 1 si no es cero
        BEQ testReb
        DEC CONT_ROC
testReb:
        TST Cont_REB            ; CONT_REB se decrementa en 1 si no es cero
        BEQ Return_RTI
        DEC Cont_REB
Return_RTI:
        RTI

;***********************************************************************************************************************************
;                                                       CALCULAR
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina atiende las interrupciones del puerto H. Cuando se presion PH3 (S1), se reinicia TICK_MED, cuando se presiona
; PH0 (S2) se obtiene el valor de la velocidad y se guarda en una variable temporal. Por ultimo, cuando se libera PH3 (S1), se
; calcula el valor de la longitud del tubo, este se guarda en la variable LONG y en VELOC se guarda el valor de la velocidad
; calculado anteriormente.
;
; Parametros de Entrada
;       CONT_REB: Variable recibida por memoria. Se utilizan para la supresion de rebotes.
;       TICK_MED: Variable se recibe por memoria el valor actual del contador.
;
; Parametros de Salida
;       LONG: Se devuelve por memoria el valor de la velocidad calculado.
;       VELOC: Se devuelve por memoria el valor de la longitud calculado.
;       Banderas (Banderas,$80):
;               DisplayCalc: Se cambia su valor para indicar cuando se debe desplegar en la LCD el mensaje de espera
;       CONT_REB: En caso de ser cero, se devuelve por memoria su valor recargado.
;       TICK_MED: Cuando se inicia una nueva medicion, se devuelve por memoria con un valor de cero.
;***********************************************************************************************************************************


CALCULAR:
        BRSET PIFH,$01,PTH0     ; Se revisa si se presiono el boton PH0 o PH3
        BRSET PIFH,$08,PTH3
        BRA RET_PTH
PTH0:                         	; Interrupcion S2
        MOVB #$01,PIFH          ; Se baja la bandera
        TST Cont_Reb
        BNE Ret_PTH
        LDX TICK_MED
        LDD #2289               ; D=50/T
        IDIV
        TFR X,A
        STAA VELOC_TEMP         ; VELOC_TEMP = 50*f/Ticks
        MOVB 10,CONT_REB
        BRA RET_PTH
PTH3:
        MOVB #$08,PIFH          ; Se baja la bandera
        TST Cont_Reb
        BNE Ret_PTH
        BRCLR PPSH,$08,primer_int
                                ; Segunda interrupcion de S1
        BCLR PPSH,$08        	; Flanco negativo
        LDD TICK_MED      	; Calcular Longitud
        LDX #46
        IDIV
        TFR X,A
        LDAB VELOC_TEMP
        MUL
        STAB LONG               ; LONG = (VELOC*Ticks)/f
        MOVB VELOC_TEMP,VELOC   ; Se escribe valor de velocidad
        CLR VELOC_TEMP
        MOVW #$0000,TICK_MED
        MOVB 10,CONT_REB
        BRA RET_PTH
Primer_int:                     ; Primera interrupcion de S1
        BSET Banderas,$80       ; Display mensaje calculando
        BSET PPSH,$08         	; Flanco positivo
        MOVW #$0000,TICK_MED
        MOVB 10,CONT_REB
RET_PTH:
        RTI

;***********************************************************************************************************************************
;                                                       TCNT_ISR
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina atiende la interrupcion por Timer Overflow, en esta se incrementa la variable TICK_MED, utilizada para calcular
; la velocidad y longitud del tubo, tambien se lleva el control de cambios de mensajes en las pantallas LCD y de 7 segmentos.
; Para llevar a cabo esa funcion, se decrementan las variables TICK_EN y TICK_DIS, en el momento en que TICK_EN sea cero, se pone
; en 1 la bandera Pant_Flg para desplegar el mensaje y cuando TICK_DIS sea cero, la bandera Pant_Flg se pone en cero para quitar
; los mensajes de las pantallas.
;
; Parametros de Entrada
;       TICK_DIS: Variable recibida por memoria. Se utiliza para el control de mensajes en las pantallas.
;       TICK_EN: Variable recibida por memoria. Se utiliza para el control de mensajes en las pantallas.
;       TICK_MED: Variable se recibe por memoria con el valor actual del contador.
;
; Parametros de Salida
;       TICK_MED: Se devuelve por memoria el valor incrementado. Variable utilizada para el calculo de la velocidad y
;        la longitud del tubo.
;       TICK_EN: Se devuelve por memoria con un valor de 0 o decrementado en 1. Variable utilizada para el control de
;        despliegue de mensajes en las pantallas LCD y de 7 segmentos.
;       TICK_DIS: Se devuelve por memoria con un valor de 0 o decrementado en 1. Variable utilizada para el control de
;        despliegue de mensajes en las pantallas LCD y de 7 segmentos.
;       Banderas (Banderas+1,$80):
;               Pant_flg: Se devuelve por memoria e indica si se debe desplegar o no los mensajes en las pantallas LCD
;                y de 7 segmentos.
;***********************************************************************************************************************************

TCNT_ISR:
        BSET TFLG2,$80
        LDD TICK_MED        	; Se incrementa la cuenta de ticks para el calculo de VELOC y  LONG
        ADDD #1
        STD TICK_MED
        LDX TICK_EN             ; Se decrementa Tick_EN en caso de que sea distinto de cero
        CPX #0
        BEQ TE_0
        DEX
        STX TICK_EN
Cont_tcnt:
        LDX TICK_DIS            ; Se decrementa Tick_EN en caso de que sea distinto de cero
        CPX #0
        BEQ TD_0
        DEX
        STX TICK_DIS
        BRA Ret_tcnt
TE_0:                         	; En el caso de que TICK_EN = 0
        BSET Banderas+1,$08     ; Pant_flag=1
        BRA Cont_tcnt
TD_0:                           ; En el caso de que TICK_DIS = 0
        BCLR Banderas+1,$08     ; Pant_flag=0
Ret_tcnt:
        RTI

;***********************************************************************************************************************************
;                                                       ATD_ISR
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de promediar los valores obtenidos del convertidor ATD. Los datos se reciben del potenciometro, de
; este se hacen 5 lecturas, por lo que el promedio se realiza con estas y se guarda en la variable POT. Luego, por medio de una
; progresion lineal se calcula el valor de BRILLO:
;                                    BRILLO = (20*POT)/255
; Parametros de Entrada
;       ADR00H-ADR04H: Se reciben los datos a promediar por medio de de las direcciones de memoria de los registros de datos del
;                       convertidor ATD.
;
; Parametros de Salida
;       BRILLO: Devuelve por memoria el valor de BRILLO.
;***********************************************************************************************************************************

ATD_ISR:
        LDX #5                  ; Se calcula el promedio de las mediciones
        LDD ADR00H              ; Se suman las 5 lecturas
        ADDD ADR01H
        ADDD ADR02H
        ADDD ADR03H
        ADDD ADR04H
        IDIV                    ; Se dicide la suma entre 5
        TFR X,D
        STAB POT                ; Se guarda el promedio en POT
        LDAA #20
        MUL
        LDX #255
        IDIV
        TFR X,D
        STAB BRILLO             ; BRILLO = (20*POT)/255
        RTI

;***********************************************************************************************************************************
;                                                      SUBRUTINAS GENERALES
;***********************************************************************************************************************************

;***********************************************************************************************************************************
;                                                        STOP
;***********************************************************************************************************************************
; Descripcion
; Modo de operacion en el que no se hacen mediciones y los displays de 7 segmentos se encuentran apagados.
; Parametros de Salida
;       BIN1: Se ponen en $BB para apagar los display de 7 segmentos (DSP3,DSP4)
;       BIN2: Se ponen en $BB para apagar los display de 7 segmentos (DSP1,DSP2)
;       LEDS: Se enciende el LED correspondiente al modo stop
;***********************************************************************************************************************************
STOP:
        MOVB #$BB,BIN1          	; Apagar display 7 segmentos
        MOVB #$BB,BIN2
        MOVB #$04,LEDS                  ; Encender Led de Stop
        RTS

;***********************************************************************************************************************************
;                                                       CONFIG
;***********************************************************************************************************************************
; Descripcion
; Modo de operacion en el que se lee LengthOK del teclado matricial, se revisa si el valor ingresado es valido
; y en caso de serlo se respalda en ValorLength y se enscribe en BIN1 para desplegarlo en los displays de 7 segmentos.
; En este modo se puede modificar LengthOK todas las veces que sea necesario.
; Parametros de Entrada:
;       LengthOK: Valor ingresado por el usuario
;       Banderas:
;                ArrayOK (Banderas+1,$04):  Indica si se ingreso un valor por medio del teclado matricial
;       Cont_TCL: Se recibe para reiniciar y que asi se pueda sobreescribir Num_Array
;       Num_Array: Se recibe para borrar su contenido y que pueda escribirse otro valor
;
; Parametros de Salida
;       BIN1: Se escribe el valor de LengthOK en binario si este es un valor valido
;       TICK_EN: Se reinicia
;       TICK_DIS: Se reinicia

;***********************************************************************************************************************************
CONFIG:
        CLR TICK_EN
        CLR TICK_DIS
        MOVB LengthOK,BIN1               ; Se despliega el valor de Lengthok en los 7 segmentos
        JSR Tarea_Teclado                ; Se ArrayOK del teclado
        BRCLR Banderas+1,$04,Ret_config2 ;
        JSR BCD_BIN
        LDAA ValorLength                 ; Se revisa si ValorLength se encuentra en el rango valido
        CMPA #70
        BMI No_valido
        CMPA #101
        BPL No_valido
        MOVB ValorLength,LengthOK       ; Se respalda el valor de LengthOK en ValorLength
        MOVB ValorLength,BIN1           ; Se muestra VelorLength en los display 7 segmentos
        BRA Ret_config
No_valido:
        CLR ValorLength
Ret_config:
        BCLR Banderas+1,$04
        CLR Cont_TCL                    ; Borrar estructuras de datos de TareaTeclado
        MOVW #$FFFF,Num_array
Ret_config2:
        RTS

;***********************************************************************************************************************************
;                                                       SELECT
;***********************************************************************************************************************************
; Descripcion
; Modo de operacion en el que se realizan las mediciones de velocidad y longitud de los tubos. Ademas se despliega en la
; pantalla LCD si la longitud es valida o no y en los displays de 7 segmentos se muestran los valores de la velocidad y de la
; longitud.
; Parametros de Entrada:
;       VELOC: Valor de velocidad calculado
;       Msg_espera_L1: Mensaje para desplegar en la LCD
;       Msg_espera_L2: Mensaje para desplegar en la LCD
;       Banderas:
;               Display_Calc (Banderas,$80): Indica si ya se actualizo el mansaje de espera en la pantalla LCD
;***********************************************************************************************************************************
SELECT:

        BRCLR Banderas,$80,cont_select   ; Se revisa si ya se actualizo el mensaje en la LCD
        BCLR Banderas,$80
        LDX #Msg_espera_L1              ; Se pone mensaje de espera en LCD
        LDY #Msg_espera_L2
        JSR Cargar_LCD
        
cont_select:
        TST VELOC                       ; Se revisa si VELOC=0
        BEQ RET_SELECT
        JSR PANT_CTRL
RET_SELECT:
        RTS


;***********************************************************************************************************************************
;                                                       PANT_CTRL
;***********************************************************************************************************************************
; Descripcion
; Se encarga del control de las pantallas LCD y de 7 segmentos. Primero se revisa si el valor de la velocidad esta en el rango valido,
; si no lo esta despliega un mensaje de alerta en la pantalla LCD y en los displays de 7 segmentos muestra guiones. En el caso de que
; la velocidad tenga un valor valido, se procede a desplegar el valor de la longitud y de la velocidad en los displays de 7 segmentos
; y en la pantalla LCD se muestra si la longitud es deficiente o si es correcta, en este utlimo caso, se activa el rele.
; Parametros de Entrada:
;       Cont_ROC: Se utiliza para controlar el tiempo de encendido del rele
;       VELOC: Valor de la velocidad calculada
;       LONG: Valor de la longitud calculada
;       Banderas:
;                Calc_Ticks (Banderas+1,$10): Indica si ya se calculo el valor de tick_en y tick_dis
;               Pant_flg (Banderas+1,$08): Cambia su valor si tick_en o tick_dis alcanzan el valor de cero,
;                                               se utiliza para calcular el tiempo que se deben desplegar los
;                                               mensajes y valores en las pantallas.
;       Msg_alerta_L1: Mensaje para la LCD
;       Msg_alerta_L2: Mensaje para la LCD
;       Msg_longd_L1: Mensaje para la LCD
;       Msg_longd_L2: Mensaje para la LCD
;       Msg_longc_L1: Mensaje para la LCD
;       Msg_longc_L2: Mensaje para la LCD
;       LengthOK: Valor ingresado por el usuario, se utiliza para comparar el valor de LONG
;
; Parametros de Salida
;       TICK_EN: Se calcula su valor de la siguiente manera
;                                Tick_en = tiempo*frecuencia
;                                        = distancia/velocidad * frecuencia
;                                        = (250-long/2)/VELOC * 46 Hz
;       TICK_DIS: Se calcula su valor de la siguiente manera
;                                Tick_dis = tiempo*frecuencia
;                                        = distancia/velocidad * frecuencia
;                                        = (250/VELOC) * 46 Hz
;       BIN1: Se escribe el valor en binario que se desea poner en los displays de 7 segmentos (DSP3,DSP4)
;             Si se desea mantener apagados se escribe $B, si se desea un guion se debe escribir $A
;       BIN2: Se escribe el valor en binario que se desea poner en los displays de 7 segmentos (DSP1,DSP2)
;             Si se desea mantener apagados se escribe $B, si se desea un guion se debe escribir $A
;***********************************************************************************************************************************
PANT_CTRL:

        BCLR PIEH,$0F              	; Se deshabilitan interrupciones PTH
        TST CONT_ROC
        BNE ROC_ON
        BCLR PORTE,$04                 	; Apagar rociador
ROC_ON:
        LDAA VELOC                  	; Se revisa si VELOC se encuentra en el rango valido
        CMPA #10
        BMI V_no_valido
        CMPA #51
        BPL V_no_valido
        BRSET Banderas+1,$10,No_calc_int  	; Se revisa si ya se calculo tick_en y tick_dis
        BSET Banderas+1,$10
        LDAB LONG                   	; Calcular Tick_EN
        CLRA
        LDX #2
        IDIV                       	; LONG/2
        TFR X,D
        STAB V_temp                  	; Se guarda en variable temporal
        LDAB VELOC
        CLRA
        TFR D,X
        LDAB #250
        SUBB V_TEMP               	; 250 - V
        LDAA #46
        MUL                             ; (250 - v)*46
        IDIV
        STX Tick_En            		; Tick_en = (250-long/2)/veloc *f
        LDAB VELOC
        CLRA
        TFR D,X
        LDD #11500                      ; f * d = 46 * 250
        IDIV
        STX Tick_Dis                 	; Tick_dis = (250/VELOC) * 46 Hz
        RTS
NO_CALC_INT:
        BRA NO_CALC
V_no_valido:                            ; En caso de velocidad no valida
        LDAA #$AA
        CMPA BIN1
        BEQ Cmp_PantF
        CLR Tick_En
        MOVW #92,Tick_dis
        MOVB #$AA,BIN1
        MOVB #$AA,BIN2
        BSET Banderas+1,$08             ; PantFlag=1
        LDX #Msg_alerta_L1             	; Se pone mensaje de alerta en LCD
        LDY #Msg_alerta_L2
        JSR Cargar_LCD
        BCLR Banderas,$80               ; Se borra bandera para que no se imprima mensaje de espera
        BRA Ret_Pant_int
Cmp_PantF:
        BRCLR Banderas+1,$08,PantF_0  	; Se revisa Pant_flg
        BRA Ret_Pant_int
No_calc:
        BRCLR Banderas+1,$08,Flag_es0   ; Se revisa Pant_flg
        LDAA BIN1
        CMPA #$BB
        BNE Ret_Pant
        LDAA LONG                       ; Si Pant_Flg=1 y BIN1 = $BB se compara LONG con LengthOK
        CMPA LengthOk
        BPL LenValido
NoLenVal:
        LDX #Msg_longd_L1               ; Se pone mensaje de longitud deficiente en LCD
        LDY #Msg_longd_L2
        JSR Cargar_LCD
        MOVB LONG,BIN1                  ; Se desplega el valor de veloc y de long en los displays de 7 segmentos
        MOVB VELOC,BIN2
Ret_pant_int:
        BRA Ret_Pant
LenValido:
        LDX #Msg_longc_L1               ; Se pone mensaje de longitud correcta en LCD
        LDY #Msg_longc_L2
        JSR Cargar_LCD
        MOVB LONG,BIN1                  ; Se desplega el valor de veloc y de long en los displays de 7 segmentos
        MOVB VELOC,BIN2
        BSET PORTE,$04                  ; Activar rele
        MOVB #200,CONT_ROC              ; Se carga contador para activar el rele por 0.2 s
        BRA Ret_Pant
Flag_es0:
        LDAA BIN1
        CMPA #$BB
        BEQ Ret_Pant
PantF_0:
        LDX #Msg_inicial_L1          	; Se pone mensaje inicial en LCD
        LDY #Msg_inicial_L2
        JSR Cargar_LCD
        MOVB #$BB,BIN1                  ; Apagar 7 segmentos
        MOVB #$BB,BIN2
        BSET PIEH,$0F                   ; Habilitar interrupciones PTH
        BCLR Banderas+1,$10
        CLR VELOC
        BRA Ret_Pant
Ret_Pant:
        RTS

;***********************************************************************************************************************************
;                                                       BCD_7SEG
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de colocar en las variables DISP1, DISP2, DISP3 y DISP4 el numero correspondiente en BCD para cada uno
; de los displays de 7 segmentos. Para esto se lee la parte baja y la parte alta de BCD1 y BCD2 y se asignan de la siguiente forma:
;               BCD1_BAJA -> DISP4
;               BCD1_ALTA -> DISP3
;               BCD2_BAJA -> DISP2
;               BCD2_BAJA -> DISP1
; Parametros de Entrada
;       BCD1: Variable se recibe por memoria. Contiene en BCD el numero a despliega en DSP3 y DSP4.
;       BCD2: Variable se recibe por memoria. Contiene en BCD el numero a despliega en DSP1 y DSP2.
;       SEGMENT: Se recibe por memoria por medio de esta tabla. En esta se encuentran los segmentos asociados a los digitos del
;               0 al 9, asi como de guion y apagado.
;
; Parametros de Salida
;       DISP1, DISP2, DISP3, DISP4: Se devuelve por memoria por estas variables. Contienen el numero correspondiente a cada display.
;***********************************************************************************************************************************
BCD_7SEG:
        LDX #SEGMENT
        LDAA BCD1            	; Se lee BCD1
        ANDA #$0F            	; Se lee parte baja de BCD1
        MOVB A,X,DISP4          ; Se carga en DISP4 el valor correspondiente
        LDAA BCD1
        ANDA #$F0         	; Se lee parte alta de BCD1
        LSRA
        LSRA
        LSRA
        LSRA
        MOVB A,X,DISP3          ; Se carga en DISP3 el valor correspondiente

        LDAA BCD2            	; Se lee BCD2
        ANDA #$0F             	; Se lee parte baja de BCD2
        MOVB A,X,DISP2          ; Se carga en DISP2 el valor correspondiente
        LDAA BCD2
        ANDA #$F0            	; Se lee parte alta de BCD2

        LSRA
        LSRA
        LSRA
        LSRA
        MOVB A,X,DISP1          ; Se carga en DISP1 el valor correspondiente
END_DISP1:
        RTS

;***********************************************************************************************************************************
;                                                       CONV_BIN_BCD
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de convertir los valores de BIN1 y BIN2 a BCD, esto lo hace llamando a la subrutina BIN_BCD. Los valores
; resultantes son leidos en BCD_L y se guardan en BCD1 o BCD2 segun corresponda. Ademas, se encarga de escribir $B si el display debe
; apagarse y en caso de leer $AA en BIN1 o BIN2, se escribe $AA en BCD1 o BCD2 segun corresponda.
;
; Parametros de Entrada
;       BIN1: Se pasa por memoria por esta variable. Es el numero en binario a desplegar en DSP3 y DSP4.
;       BIN2: Se pasa por memoria por esta variable. Es el numero en binario a desplegar en DSP1 y DSP2.
;       BCD_L: Se pasa por memoria por esta variable. Es el numero en BCD que devuelve la subrutina BIN_BCD.
;
; Parametros de Salida
;       BCD1: Se devuelve por memoria por esta variable. Es el numero en BCD a desplegar en DSP3 y DSP4.
;       BCD2: Se devuelve por memoria por esta variable. Es el numero en BCD a desplegar en DSP1 y DSP2.
;***********************************************************************************************************************************
CONV_BIN_BCD:
        LDAA BIN1
        CMPA #$BB               ; Se revisa si BIN = $BB
        BEQ NO_CONV1
        CMPA #$AA               ; Se revisa si BIN = $AA
        BEQ NO_CONV1_A
        JSR BIN_BCD
        LDAA BCD_L
        ANDA #$F0           	; Se revisa si hay cero a la izquierda
        BNE No_es_cero1
        LDAA BCD_L
        ADDA #$B0
        STAA BCD_L
        BRA No_es_cero1
NO_CONV1:                       ; En caso de que BIN = $BB
        MOVB #$BB,BCD_L
        BRA No_es_cero1
NO_CONV1_A:                     ; En caso de que BIN2 = $AA
        MOVB #$AA,BCD_L
No_es_cero1:
        MOVB BCD_L,BCD1
        LDAA BIN2
        CMPA #$BB               ; Se revisa si BIN2 = $BB
        BEQ NO_CONV2
        CMPA #$AA               ; Se revisa si BIN2 = $AA
        BEQ NO_CONV2_A
        JSR BIN_BCD
        LDAA BCD_L
        ANDA #$F0       	; Se revisa si hay cero a la izquierda
        BNE No_es_cero2
        LDAA BCD_L
        ADDA #$B0
        STAA BCD_L
        BRA No_es_cero2
NO_CONV2:                       ; En caso de que BIN2 = $BB
        MOVB #$BB,BCD_L
        BRA No_es_cero2
NO_CONV2_A:
        MOVB #$AA,BCD_L         ; En caso de que BIN2 = $AA
No_es_cero2:
        MOVB BCD_L,BCD2
        RTS

;***********************************************************************************************************************************
;                                                       BIN_BCD
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de convertir de binario a BCD el numero de 8 bits recibido por el acumulador A, el resultado en BCD
; se devuelve por medio de la variable BCD_L.
;
; Parametros de Entrada
;       Numero Binario: El numero en binario que se desea pasar a BCD se recibe por medio del acumulador A.
;
; Parametros de Salida
;       BCD_L: Se devuelve por memoria por esta variable. Es el resultado de la conversion a BCD.
;***********************************************************************************************************************************
BIN_BCD:
        CLR BCD_L
        LDY #7
LOOP_BIN_BCD:
        LSLA              	; Se hace un desplazamiento en el numero binario
        ROL BCD_L           	; Se desplaza la parte menos significativa
        STAA LOW                ; Se respalda el numero en binario desplazado en LOW
        LDAA BCD_L
        ANDA #$0F               ; Se carga en B la parte baja del numero en BCD
        CMPA #$5             	; Si es menor a 5 el valor se mantiene
        BMI MENOR
        ADDA #$3           	; Se le suma 3 en caso de ser mayor o igual a 5
MENOR:
        LDAB BCD_L
        ANDB #$F0             	; Se carga en B la parte alta del numero en BCD
        CMPB #$50            	; Si esa parte es menor que 5 el valor se mantiene
        BMI MENOR2
        ADDB #$30               ; Si es mayor o igual, se le suma 3
MENOR2:
        ABA                	; Se suman la parte mas significativa y la menos significativa del numero
        STAA BCD_L
        LDAA LOW
        DBNE Y,LOOP_BIN_BCD    	; En el caso de haber desplazado 7 bits se sale del loop
        LSLA                 	; Se hace el desplazamiento final en el numero binario;
        ROL BCD_L               ; Se hace el desplazamiento final en el numero en BCD
        RTS

;***********************************************************************************************************************************
;                                                       BCD_BIN
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de convertir de BCD a binario el numero guardado el Num_Array. Luego se guarda el resultado en BCD en
; la variable ValorLength.
;
; Parametros de Entrada
;       Num_Array: El numero en BCD que se desea pasar a binario se recibe por medio de Num_Array.
;
; Parametros de Salida
;       ValorLength: Se devuelve por memoria por esta variable. Es el resultado de la conversion a binario.
;***********************************************************************************************************************************

BCD_BIN:

        LDD Num_Array        	; Se carga en D el numero en BCD
        CPD #$FFFF
        BEQ RET_BCD_BIN
        ANDB #$0F               ; Se carga en B la parte baja del byte menos significativo
        STAB ValorLength   	; Se guarda en ValorLength el resultado de procesar los primeros 4 bits
        LDD Num_Array
        ANDA #$0F               ; Se carga en B la parte baja del byte menos significativo
        LDAB #10
        MUL
        LDAA ValorLength
        ABA
        STAA ValorLength  	; Se multiplican los bits seleccionados por la potencia de 10 correspondiente
RET_BCD_BIN:
        RTS

;***********************************************************************************************************************************
;                                                       CARGAR_LCD
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de actualizar el mensaje desplegado en la pantalla LCD cada vez que la bandera CambMod se active,
; los mensajes a desplegar son recibidos por medio de X y Y.
;
; Parametros de Entrada
;       Mensaje Linea 1: La linea 1 del mensaje a desplegar se recibe por medio del indice X.
;       Mensaje Linea 2: La linea 2 del mensaje a desplegar se recibe por medio del indice Y.
;       D2mS, D40uS: Se recibe por memoria por medio de estas variables la cantidad respectiva de ticks para provocar un
;	delay de 2 ms o	de 40 us.
;       ADD_L1: Se recibe por medio de esta variable el comando para anadir la linea 1.
;       ADD_L2: Se recibe por medio de esta variable el comando para anadir la linea 2.
;
; Parametros de Salida
;       No tiene parametros de salida
;***********************************************************************************************************************************
Cargar_LCD:
        PSHX
        LDX #iniDsp
        LDAB 1,X+
LOOP_LCD:
        LDAA 1,X+
        JSR SEND_COMMAND          	; Se envian lo comandos de iniDsp
        MOVB D40uS,Cont_Delay
        JSR Delay                       ; Delay de 40us
        DBNE B, LOOP_LCD
        LDAA clear_LCD
        JSR SEND_COMMAND                ; clr_LCD
        MOVB D2mS,Cont_Delay
        JSR Delay                     	; Delay de 2ms
        PULX
        LDAA ADD_L1
        JSR SEND_COMMAND             	; L1
        MOVB D40uS,Cont_Delay
        JSR DELAY                   	; Delay de 40us
Loop_cargar:
        LDAA 1,X+
        CMPA #EOM
        BEQ Cont3
        JSR SEND_DATA                   ; Se actualiza mensaje linea 1
        MOVB D40uS,Cont_Delay
        JSR Delay                     	; Delay de 40us
        BRA Loop_cargar

Cont3:  LDAA ADD_L2
        JSR SEND_COMMAND             	; L2
        MOVB D40uS,Cont_Delay
        JSR Delay                   	; Delay de 40 us
Loop2_cargar:
        LDAA 1,Y+
        CMPA #EOM
        BEQ Return_Cargar
        JSR SEND_DATA               	; Se actualiza mensaje linea 2
        MOVB D40uS,Cont_Delay
        JSR Delay                   	; Delay de 40us
        BRA Loop2_Cargar
Return_Cargar:
        RTS


;***********************************************************************************************************************************
;                                                       SEND_DATA
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de enviar datos a la pantalla LCD
;
; Parametros de Entrada
;       Dato: El dato a enviar se recibe por medio del acumulador A.
;       D260uS: Se recibe por memoria por medio de esta variable la cantidad de ticks necesarios para provocar un
;	delay 260 us.
;
; Parametros de Salida
;       No tiene parametros de salida
;***********************************************************************************************************************************
SEND_DATA:
        PSHA
        ANDA #$F0
        LSRA
        LSRA
        STAA PORTK
        BSET PORTK,$01          ; Se pone RS=1
        BSET PORTK,$02          ; Se pone EN=1
        MOVB D260uS,Cont_Delay  ; Delay de 260 us
        JSR Delay
        BCLR PORTK,$02          ; Se pone EN=0
        PULA
        ANDA #$0F
        LSLA
        LSLA
        STAA PORTK
        BSET PORTK,$01          ; Se pone RS=1
        BSET PORTK,$02          ; Se pone EN=1
        MOVB D260uS,Cont_Delay  ; Delay de 260 us
        JSR Delay
        BCLR PORTK,$02          ; Se pone EN=0
        RTS

;***********************************************************************************************************************************
;                                                       SEND_COMMAND
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de enviar comandos a la pantalla LCD
;
; Parametros de Entrada
;       Comando: El comando a enviar se recibe por medio del acumulador A.
;       D260uS: Se recibe por memoria por medio de esta variable la cantidad de ticks necesarios para provocar un
;	delay 260 us.
;
; Parametros de Salida
;       No tiene parametros de salida
;***********************************************************************************************************************************
SEND_COMMAND:
        PSHA
        ANDA #$F0
        LSRA
        LSRA
        STAA PORTK
        BCLR PORTK,$01          ; Se pone RS=0
        BSET PORTK,$02          ; Se pone EN=1
        MOVB D260uS,Cont_Delay  ; Delay de 260 us
        JSR Delay
        BCLR PORTK,$02          ; Se pone EN=0
        PULA
        ANDA #$0F
        LSLA
        LSLA
        STAA PORTK
        BCLR PORTK,$01          ; Se pone RS=0
        BSET PORTK,$02          ; Se pone EN=1
        MOVB D260uS,Cont_Delay  ; Delay de 260 us
        JSR Delay
        BCLR PORTK,$02          ; Se pone EN=0
        RTS

;***********************************************************************************************************************************
;                                                       DELAY
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de provocar un retardo basado en Cont_Delay. En caso de esta variable ser distinta de cero, entonces
; se llama a si misma. Cont_Delay es decrementado en la subrutina RTI_ISR y cuando el contador llegue a cero entonces DELAY retorna.
;
; Parametros de Entrada
;       Cont_Delay: Por medio de esta variable se recibe cuanto delay se quiere generar. Si es cero la subrutina retorna, en caso de no
;       serlo, entonces la subrutina se llama a si misma.
;
; Parametros de Salida
;       No tiene parametros de salida
;***********************************************************************************************************************************
Delay:
        TST Cont_Delay
        BNE Delay
        RTS
;***********************************************************************************************************************************
;                                                       TAREA_TECLADO
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina será la encargada de llamar a la subrutina MUX_TECLADO para leer la tecla presionada. Tambien se encarga de la
; supresion de rebotes y se encarga de que si una tecla se retiene, solo sea leida una vez. Una vez leida la tecla, se decide
; si esta es valida o no, esto se hace comparando dos lecturas despues de la supresion de rebotes. Por ultimo, se llama a la
; subrutina FORMAR_ARRAY para escribir la tecla que esta lista.
;
; Parametros de Entrada
;       Cont_REB: Se recibe por medio de memoria. Esta variable se utiliza para la supresion de rebotes.
;       Tecla: Se recibe por medio de memoria. Guarda el valor de la tecla presionada en el teclado.
;       Tecla_IN: Se recibe por medio de memoria. Guarda el valor de la tecla leida antes de suprimir rebotes.
;       Banderas (Banderas+1,$02):
;                               TCL_Leida: Indica si se ha leido una tecla.
;       Banderas (Banderas+1,$01):
;                               TCL_Lista: Indica si el valor de la tecla es valido.
;
; Parametros de Salida
;       Num_Array: Se devuelve el valor ingresado por el usuario a traves de este arreglo.
;***********************************************************************************************************************************

TAREA_TECLADO:
        TST Cont_Reb                    ; Supresion a rebotes
        BNE Retornar                    ; No han terminado los rebotes
        JSR MUX_TECLADO
        BRSET Tecla,$FF,NoPres          ; No se ha presionado una tecla
        BRCLR Banderas+1,$02,NoProcesada  ; No se ha procesado tecla
        LDAA Tecla
        CMPA Tecla_IN
        BEQ Lista                       ; Valor de tecla valido
        MOVB #$FF,Tecla
        MOVB #$FF,Tecla_IN
        BCLR Banderas+1,$02            	; Se limpia bandera TCL_Leida
        BCLR Banderas+1,$01         	; Se limpia bandera TCL_Lista
        BRA RETORNAR
NoPres:                                 ; No se ha presionado tecla
        BRCLR Banderas+1,$01,RETORNAR 	; Si no se tiene ninguna tecla lista se retorna
        BCLR Banderas+1,$01
        BCLR Banderas+1,$02
        JSR FORMAR_ARRAY                ; Si hay tecla lista, se llama a subrutina FORMAR_ARRAY
        BRA RETORNAR
NoProcesada:                            ; Si todavia no se ha procesado la tecla
        MOVB Tecla,Tecla_IN
        BSET Banderas+1,$02
        MOVB 10,CONT_REB
        BRA RETORNAR
Lista:
        BSET Banderas+1,$01

RETORNAR:
        RTS

;***********************************************************************************************************************************
;                                                       MUX_TECLADO
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina se encarga de leer el teclado matricial. Este debe leerse de manera iterativa enviando uno de los 4 patrones
; ($EF, $DF, $BF, $7F) al puerto A. Para leer el teclado se busca cual bit de la parte baja del puerto A esta en cero y asi se
; identifica la tecla presionada. El valor de la tecla leida se regresa a TAREA_TECLADO por medio de la variable TECLA.
;
; Parametros de Entrada
;       Teclas: Se recibe por medio de esta tabla, los valores de cada tecla del teclado.
;
; Parametros de Salida
;       Tecla: Se devuelve el valor de la tecla presionada por medio de esta variable.
;***********************************************************************************************************************************

MUX_TECLADO:
        LDX #Teclas
        MOVB #$EF,Patron                  ; Primer patron
        CLRB
LOOP_MUX:
        MOVB Patron,PORTA                 ; Se pone el patron en el puerto A
        NOP
        NOP
        NOP
        NOP
        NOP
        BRCLR PORTA,$01,FOUND              ; Se revisa si fue presionada una tecla de la primera columna
        INCB
        BRCLR PORTA,$02,FOUND              ; Se revisa si fue presionada una tecla de la segunda columna
        INCB
        BRCLR PORTA,$04,FOUND              ; Se revisa si fue presionada una tecla de la tercera columna
        INCB
        LSL Patron                         ; Se desplaza a la izquierda para obtener el nuevo patron
        BRSET Patron,$F0,BORRAR
        BRA LOOP_MUX
FOUND:
        MOVB B,X,Tecla                     ; Se guarda el valor de la tecla
        RTS
BORRAR:
        MOVB #$FF,Tecla                    ; Se borra la tecla
        RTS


;***********************************************************************************************************************************
;                                                       FORMAR_ARRAY
;***********************************************************************************************************************************
; Descripcion
; Esta subrutina recibe el valor de la tecla presionada en la variable Tecla_IN y se encarga de colocar estos valores recibidos en
; el arreglo Num_Array de manera ordenada, el tamano del arreglo esta limitado por el valor de MAX_TCL. Se utiliza una variable
; llamada Cont_TCL para guardar la posicion actual en Num_Array. En el caso de que se ingrese la tecla $0E (Enter), se hace ARRAY_OK=1
; lo que indica que finalizo el Num_Array. Si lo que se recibe en Tecla_IN es $0B (Borrar), entonces se debe poner $FF para borrar
; la ultima posicion del arreglo Num_Array. Si se presiona Borrar o Enter como la primera tecla deben ser ignoradas y si ya se alcanzo
; el tamano maximo de Num_Array, entonces las unicas teclas validas serian Borrar y Enter.
;
; Parametros de Entrada
;       Cont_TCL: Por medio de esta variable se recibe la posicion actual en el arreglo Num_Array.
;       MAX_TCL: Por medio de esta constante se recibe el valor del tamano maximo de Num_Array.
;       Tecla_IN: Por medio de esta variable se recibe el valor de la tecla ingresada.
;
; Parametros de Salida
;       Num_Array: Se devuelve por medio de este arreglo los valores de las teclas presionadas.
;       Banderas (Banderas+1,$04):
;                               Array_OK: Indica si se termino de escribir el numero en el arreglo.
;***********************************************************************************************************************************

FORMAR_ARRAY:
        LDY #Num_Array
        LDAA Tecla_IN
        LDAB CONT_TCL
        CMPB MAX_TCL               	; Se revisa si ya se lleno el arreglo
        BEQ TCL_BORRAR                  ; En caso de estar lleno hace branch
        CMPB #0
        BEQ TCL_BORRAR_ENTER            ; Hace branch si es la primera tecla
        CMPA #$0B
        BEQ REGRESAR_PTR                ; Hace branch si la tecla es $0B
        CMPA #$0E
        BEQ LISTO                       ; Hace branch si la tecla es $0E
VALOR_ARREGLO:
        MOVB Tecla_IN,B,Y               ; Se guarda el valor de la tecla en el arreglo
        INCB
        STAB CONT_TCL
BORRAR_IN:
        MOVB #$FF,TECLA_IN              ; Se borra el valor de tecla_in
        RTS                             ; Se retorna
TCL_BORRAR:
        CMPA #$0B                       ; Si es la tecla borrar, se borra la ultima posicion
        BEQ REGRESAR_PTR
        CMPA #$0E                       ; Si es la tecla enter, se da por terminado el arreglo
        BNE BORRAR_IN
LISTO:
        BSET Banderas+1,$4           	; Array_OK = 1
        BRA BORRAR_IN
TCL_BORRAR_ENTER:                       ; En el caso de que sea la primera tecla
        CMPA #$0B
        BEQ BORRAR_IN                   ; Si es borrar se ignora
        CMPA #$0E
        BEQ BORRAR_IN                   ; Si es enter se ignora
        BRA VALOR_ARREGLO               ; En caso de no ser ni enter ni borrar, se escribe el valor en el arreglo
REGRESAR_PTR:
        DECB
        MOVB #$FF,B,Y                   ; Se borra el valor del arreglo
        STAB Cont_TCL
        BRA BORRAR_IN