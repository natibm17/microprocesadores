lo;***************************************************************************
;                       Escuela de Ingenieria Electrica
;                            Curso Microprocesadores
;                                    IE 0423
;                                    Tarea 4
;                            Natalia Bolanos B61127
;                        Ultima vez modificado 12/06/20
;****************************************************************************
;El programa se encarga de leer los valores del teclado, la secuencia leida es
;guardada en el arreglo Num_Array, el tamano de esta secuencia no debe superar
;MAX_TCL. Si se presiona la tecla enter, ya no se pueden digitar mas teclas y
; si se presiona la tecla borrar, se debe borrar la ultima tecla digitada.
; Si se presiona PH0, se borra todo el arreglo y se puede escribir nuevamente.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
#include registers.inc

                org $1000
MAX_TCL:        db 5
Tecla:          ds 1
Tecla_IN        ds 1
Cont_Reb:       ds 1
Cont_TCL:       ds 1
Patron:         ds 1
Banderas:       ds 1
Num_Array:      ds 6
Teclas:         db $01,$02,$03,$04,$05,$06,$07,$08,$09,$0B,$00,$0E


FIN_MSG:        EQU $0
MSG1    FCC "Tecla: %x" ; Mensaje inicial
        dB FIN_MSG

                org $3E70
                dw RTI_ISR
                org $3E4C
                dw PTH_ISR

;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************
                org $2000
;****************************************************************************
;              CONFIGURACION DE HARDWARE
;****************************************************************************

        MOVB #$01,PIEH ; Se habilita interrupciones al puerto H
        MOVB #$17,RTICTL ; Se carga base de tiempo
        BSET CRGINT,$80 ; Se habilitan interrupciones rti
        MOVB #$F0,DDRA ; Parte alta es salida, parte baja es entrada
        BSET PUCR,$01 ; pullup
        CLI; Se habilitan interrupciones mascarables
        
Programa:
        LDS #$3BFF
        MOVB #$FF,Tecla_IN
        MOVB #$FF,Tecla     md
        MOVB #$00,Cont_Reb
        MOVB #$00,Cont_TCL
        MOVB #$00,Patron
        MOVB #$00,Banderas
        LDAA MAX_TCL
        LDX #NUM_ARRAY-1
LOOP_CLR:
        MOVB #$FF,A,X ; Inicializar el arreglo Num_Array en $FF
        DBNE A,LOOP_CLR
LOOP_PP:
        BRSET Banderas,$4,LOOP_PP
        JSR TAREA_TECLADO
        BRA LOOP_PP
;****************************************************************************
;              TAREA_TECLADO
;****************************************************************************

TAREA_TECLADO:
        TST Cont_Reb
        BNE Retornar ; No han terminado los rebotes
        JSR MUX_TECLADO
        BRSET Tecla,$FF,NoPres ; No se ha presionado una tecla
        BRCLR Banderas,$02,NoProcesada ; No se ha procesado tecla
        LDAA Tecla
        CMPA Tecla_IN
        BEQ Lista ; Valor de tecla valido
        MOVB #$FF,Tecla
        MOVB #$FF,Tecla_IN
        BCLR Banderas,$02 ; Se limpia bandera TCL_Leida
        BCLR Banderas,$01 ; Se limpia bandera TCL_Lista
        BRA RETORNAR
NoPres:                                 ; No se ha presionado tecla
        BRCLR Banderas,$01,RETORNAR ; Si no se tiene ninguna tecla lista se retorna
        BCLR Banderas,$01
        BCLR Banderas,$02
        JSR FORMAR_ARRAY ; Si hay tecla lista, se llama a subrutina FORMAR_ARRAY
        BRA RETORNAR
NoProcesada:                ; Si todavia no se ha procesado la tecla
        MOVB Tecla,Tecla_IN
        BSET Banderas,$02
        MOVB 10,CONT_REB
        BRA RETORNAR
Lista:
        BSET Banderas,$01

RETORNAR:
        RTS
;****************************************************************************
;              MUX_TECLADO
;****************************************************************************
MUX_TECLADO:
        LDX #Teclas
        MOVB #$EF,Patron  ; Primer patron
        CLRB
LOOP_MUX:
        MOVB Patron,PORTA ; Se pone el patron en el puerto A
        NOP
        NOP
        NOP
        NOP
        NOP
        BRCLR PORTA,$01,FOUND ; Se revisa si fue presionada una tecla de la primera columna
        INCB
        BRCLR PORTA,$02,FOUND ; Se revisa si fue presionada una tecla de la segunda columna
        INCB
        BRCLR PORTA,$04,FOUND ; Se revisa si fue presionada una tecla de la tercera columna
        INCB
        LSL Patron ; Se desplaza a la izquierda para obtener el nuevo patron
        BRSET Patron,$F0,BORRAR
        BRA LOOP_MUX
FOUND:
        MOVB B,X,Tecla ; Se guarda el valor de la tecla
        RTS
BORRAR:
        MOVB #$FF,Tecla ; Se borra la tecla
        RTS


;****************************************************************************
;              FORMAR_ARRAY
;****************************************************************************

FORMAR_ARRAY:

        LDAA Tecla_IN
        PSHD
        LDX #$0
        LDD #MSG1 ; Se imprime mensaje para que ingrese un numero
        JSR [PrintF,X]
        LEAS 2,SP


        LDY #Num_Array
        LDAA Tecla_IN
        LDAB CONT_TCL
        CMPB MAX_TCL ; Se revisa si ya se lleno el arreglo
        BEQ TCL_BORRAR ; En caso de estar lleno hace branch
        CMPB #0
        BEQ TCL_BORRAR_ENTER ; Hace branch si es la primera tecla
        CMPA #$0B
        BEQ REGRESAR_PTR ; Hace branch si la tecla es $0B
        CMPA #$0E
        BEQ LISTO ; Hace branch si la tecla es $0E
VALOR_ARREGLO:
        MOVB Tecla_IN,B,Y  ; Se guarda el valor de la tecla en el arreglo
        INCB
        STAB CONT_TCL
BORRAR_IN:
        MOVB #$FF,TECLA_IN ; Se borra el valor de tecla_in
        RTS               ; Se retorna
TCL_BORRAR:
        CMPA #$0B  ; Si es la tecla borrar, se borra la ultima posicion
        BEQ REGRESAR_PTR
        CMPA #$0E ; Si es la tecla enter, se da por terminado el arreglo
        BNE BORRAR_IN
LISTO:
        BSET Banderas,$4 ; Array_OK = 1
        BRA BORRAR_IN
TCL_BORRAR_ENTER: ; En el caso de que sea la primera tecla
        CMPA #$0B
        BEQ BORRAR_IN ; Si es borrar se ignora
        CMPA #$0E
        BEQ BORRAR_IN ; Si es enter se ignora
        BRA VALOR_ARREGLO ; En caso de no ser ni enter ni borrar, se escribe el valor en el arreglo
REGRESAR_PTR:
        MOVB #$FF,B,Y ; Se borra el valor del arreglo
        DECB
        STAB Cont_TCL
        BRA BORRAR_IN
;****************************************************************************
;              SUBRUTINAS DE SERVICIO DE INTERRUPCIONES
;****************************************************************************

;****************************************************************************
;              RTI_ISR
;****************************************************************************

RTI_ISR:
        BSET CRGFLG,$80 ; Borrar bandera
LOOP_RTI:
        TST Cont_REB
        BEQ Return_RTI
        DEC Cont_REB
        BRA LOOP_RTI
Return_RTI:
        RTI

;****************************************************************************
;              PTH_ISR
;****************************************************************************

PTH_ISR:
        MOVB #$01,PIFH ; Se baja la bandera
        BCLR Banderas,$4
        LDAA MAX_TCL
        LDX #NUM_ARRAY-1
LOOP_PH0:
        MOVB #$FF,A,X ; Borrar el arreglo
        DBNE A,LOOP_PH0
        RTI