;***************************************************************************
;                       ESCUELA DE INGENIERIA ELECTRICA
;                            CURSO MICROPROCESADORES
;                                    IE 0423
;                                    TAREA 3
;                            NATALIA BOLANOS B61127
;****************************************************************************
; Este programa solicita al usuario digitar por medio de la terminal la
; cantidad de datos con raiz cuadrada entera que se deben procesar para
; obtener el valor de su raiz cuadrada. Finalmente se imprimen los valores
; calculados en la terminal.
;****************************************************************************
;               DECLARACION DE ESTRCUTURAS DE DATOS
;****************************************************************************
CR:             EQU $0D
LF:             EQU $0A
PrintF:         EQU $EE88
FIN_MSG:        EQU $0
GETCHAR:        EQU $EE84
PUTCHAR:        EQU $EE86
                org $1000
LONG:           db 8  ; Constante que indica la cantidad de datos
CANT:           ds 1  ; Variable que indica la cantidad de raices a calcular
CONT:           ds 1  ; Variable que indica la cantidad de valores encontrados
TEMP:           ds 2  ; Variable temporal utilizada para guardar una bandera indicando si ya se leyeron los 
                      ; dos digitos de CANT ingresados y utilizado en RAIZ para guardar temporalmente el valor
                      ;        del dato al que se le va a calcular la raiz
POSC:           ds 1  ; Variable que indica la posicion por la que se esta leyendo CUAD
POSD:           ds 1  ; Variable que indica la posicion por la que se esta leyendo DATOS
                org $1010
ENTERO:         ds 1  ; Arreglo para guardar los resultados de las raices calculadas
                org $1020
DATOS:          db $9,$5,$10,$3,$52,$51,$31,$40  ; Tabla con los datos a leer
                org $1030
CUAD:           db $4,$9,$10,$19,$24,$31,$40,$51,$64 ; Tabla con los numero con raices cuadradas enteras
MSG1    FCC "INGRESE EL VALOR DE CANT (ENTRE 0 Y 99): " ; Mensaje inicial
        dB FIN_MSG
MSG2    FCC "El numero 00 no es una entrada valida" ; Mensaje en caso de que la entrada digitada sea 00
        dB CR,LF
        dB FIN_MSG
MSG3    FCC "CANTIDAD DE NUMEROS ENCONTRADOS: %i"
        dB CR,LF
        dB FIN_MSG
MSG4   FCC "ENTERO:" 
        dB FIN_MSG
MSG5   FCC "%i,"
        dB FIN_MSG
MSG6   FCC "%i"
        dB FIN_MSG
MSGESP  dB CR,CR,LF,FIN_MSG

        
;****************************************************************************
;               PROGRAMA PRINCIPAL
;****************************************************************************

        org $2000
        LDS #$3BFF
        JSR LEER_CANT
        JSR BUSCAR
        JSR Print_Result
        bra *

;**************************************************************************
;                    SUBRUTINA LEER_CANT
;**************************************************************************
        
LEER_CANT:

START:
	CLR TEMP
        CLR TEMP+1
        CLR CANT
        CLRA
        LDY #$1
        LDX #$0
        LDD #MSG1 ; Se imprime mensaje para que ingrese un numero
        JSR [PrintF,X]

LOOP:
        LDX #$0
        JSR [GETCHAR,X] ; Se lee el numero ingresado
        CLRA
        CMPB #$30 ; Se pasa de ASCII a decimal
        BMI LOOP  ; Si es menor a 0
        CMPB #$3B
        BPL LOOP ; Si es mayor a A
        JSR [PUTCHAR,X] ; Se imprime el primer digito
        BRSET TEMP,$1,SAVE ; En el caso de haber leido los dos digitos se va a SAVE
        INC TEMP
        SUBB #$30
        LDAA #10
        MUL      ; Se multiplica el digito de las decenas por 10
        STAB CANT
        BRA LOOP
SAVE:
        SUBB #$30
        CLRA
        LDAA CANT
        ABA ; Se suman las decenas y las unidades
        STAA CANT ; Se guarda el resultado final en CANT
        BEQ ERROR ; En caso de que se haya ingresado 00, se produce error
        LDAB #CR
	LDX #$0
        JSR [PUTCHAR,X]
        RTS

ERROR:
  	LDX #$0
	LDD #MSG2
        JSR [PrintF,X]
        BRA START

;**************************************************************************
;                         SUBRUTINA BUSCAR
;**************************************************************************
BUSCAR:
        CLR CONT
        CLR POSD
        CLR POSC
LOOPBUS:
        LDX #DATOS
        LDY #CUAD
        LDAA POSD
        CMPA LONG
        BEQ FIN ; En el caso de haber recorrido todo el arreglo DATOS se va a FIN
        LDAA A,X ; Se lee el dato de DATOS
LOOP2:
        LDAB POSC
        LDAB B,Y ; Se lee el numero en CUAD
        CBA
        BEQ ENT ; Si el numero en CUAD y el dato son iguales, se va a ENT
        BMI MENOR ; Si el numero en CUAD es mayor al leido en DATOS se va MENOR
        INC POSC  ; Si el numero en CUAD es menor se pasa a leer el siguiente numero en CUAD
        BRA LOOP2
MENOR:
        INC POSD  ; Se pasa a leer el siguiente dato
        CLR POSC
        BRA LOOPBUS
ENT:
        PSHA
        JSR RAIZ  ; Se calcula el valor de la raiz
        PULA
        LDX #ENTERO
        LDAB CONT
        STAA B,X ; Se guarda el resultado en ENTERO
        INC CONT
        LDAB CONT
        INC POSD
        CLR POSC
	CMPB CANT
        BNE LOOPBUS
FIN:    RTS

;**************************************************************************
;                         SUBRUTINA RAIZ
;**************************************************************************
RAIZ:
        PULX
        PULA
        PSHX
        TFR A,B
        STAA TEMP+1 ; x
        CLR TEMP
LOOP3:
        TFR B,A
        TFR B,X
        TFR B,Y
        MUL ; r^2
        ADDD TEMP ; r^2 + x
        EXG D,X
        LSLD  ; 2r
        EXG X,D
        IDIV  ; X=(r^2 + x)/2r
        TFR X,B
        TFR Y,A
        CBA
        BNE LOOP3
        PULX
        PSHB
        PSHX
        RTS

;**************************************************************************
;                    SUBRUTINA PRINT_RESULT
;**************************************************************************
Print_Result:
	LDX #$0
	LDD #MSGESP ; Se imprime espacio
	JSR [PrintF,X]
	CLRA
	LDAB CONT
	PSHD
	LDX #$0
	LDD #MSG3
	JSR [PrintF,X] ; Se imprime el valor de CONT
	LEAS 2,SP
	LDX #$0
	LDD #MSGESP  ; Se imprime espacio
	JSR [PrintF,X]
	LDX #$0
	LDD #MSG4
	JSR [PrintF,X] ; Se imprime "ENTERO:"
	MOVW #ENTERO,TEMP
       
LOOPPRINT:
	CLRA
       	LDY TEMP
       	DEC CONT
       	BEQ LAST ; En el caso de ser la ultima raiz, se pasa a LAST
       	LDAB 1,Y+
       	STY TEMP
       	PSHD
       	LDX #$0
       	LDD #MSG5
       	JSR [PrintF,X] ; Se imprime el valor de la raiz
       	LEAS 2,SP
	BRA LOOPPRINT

LAST:
	CLRA
        LDAB 0,Y ; Se lee la ultima posicion de ENTERO
        PSHD
        LDX #$0
        LDD #MSG6
        JSR [PrintF,X] ; Se imprime el valor de la ultima raiz
        LEAS 2,SP
        RTS